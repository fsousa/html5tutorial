<!DOCTYPE html>
<html lang="en">
<head>
    <?php $lesson=230; ?>
    <?php include("header.php"); ?> 
    <!-- Google Ajax Api  -->

    <script src="http://www.google.com/jsapi?key=ABQIAAAAmeffKrQQUbItyWthizAaqxRgL__lfDn6TeqyC9VrS6WbMVB36xSg_wuFDXIplvs5p5ZgfV-rrnPPZQ" type="text/javascript"></script>
    <!-- Dynamic Feed Control and Stylesheet -->
    <script src="http://www.google.com/uds/solutions/dynamicfeed/gfdynamicfeedcontrol.js" type="text/javascript"></script>
    <style type="text/css">
        @import url("http://www.google.com/uds/solutions/dynamicfeed/gfdynamicfeedcontrol.css");
        
        #feed-control { 
            margin-top : 20px;
            margin-left: auto;
            margin-right: auto;
            width : 640px;
            font-size: 16px;
            color: #9CADD0;
            }
            
        .gfg-title{
            font: bold 15pt/20pt Comic Sans, Comic Sans MS, cursive;
            text-transform: capitalize;
            color: #cc0033;
            text-decoration: none;
            background-color: #F9FF9B;
            padding:10px;
        }

        .gfg-subtitle a {
          color : #005151;
        }

        .gfg-subtitle
        {
            border-bottom-width: thin;
            font: bold 10pt/20pt Georgia, Verdana, Arial;
            text-transform: none;
            color: #005151;
            background-color: #F9FF9B;
            text-decoration: none;
        }
        
        .gfg-entry
        {
            margin-top:20px;
            margin-bottom:20px;
        }

    </style>

</head>
<body>
    <script type="text/javascript">

        function LoadDynamicFeedControl() {
            var feeds = [
	{ title: 'HTML5 News',
	    url: 'http://www.google.com/reader/public/atom/user/11565553494105299636/label/HTML5%20news'
	}
	];
            var options = {
                stacked: false,
                horizontal: false,
                title: "HTML5 news",
                numResults: 30,
                displayTime: 5000
            }

            new GFdynamicFeedControl(feeds, 'feed-control', options);
        }
        // Load the feeds API and set the onload callback.
        google.load('feeds', '1');
        google.setOnLoadCallback(LoadDynamicFeedControl);
    </script>  
    
    
    <?php include("banner.php"); ?>    
    <div id="main">
        <?php include("navi.php"); ?>   
        <article>
            <header>
                <h1><?php echo $arr[$lesson][1]?></h1>
                <?php include("last_update.php") ?>            
            </header>
          
            
            <div id="feed-control">
                <span style="color: #676767; font-size: 20px; margin: 10px; padding: 4px;">Loading...</span>
            </div>
            
            <script type="text/javascript" src="http://www.google.com/reader/ui/publisher-en_GB.js"></script>
<script type="text/javascript" src="http://www.google.com/reader/public/javascript/user/11565553494105299636/label/HTML5 news?n=10&callback=GRC_p(%7Bc%3A%22blue%22%2Ct%3A%22%5C%22HTML5%20news%5C%22%22%2Cs%3A%22false%22%2Cn%3A%22true%22%2Cb%3A%22true%22%7D)%3Bnew%20GRC"></script>
        </article>
        <?php include("navi_page.php"); ?>
     </div>    
    <?php include("footer.php"); ?> 
</body>
</html>
