<?php $lesson=160; ?>
<?php include("page_header.php"); ?> 
    <article>
        <header>
            <h1><?php echo $lesson_arr[$current_id][1]?></h1>
        </header>
<script type="text/javascript"><!--
google_ad_client = "ca-pub-3162865608794255";
/* Html5 below title */
google_ad_slot = "6072872264";
google_ad_width = 728;
google_ad_height = 15;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script> 
            
            <img src="images/canvas.jpg" alt="Canvas" title="Canvas" class="clipart" style="float:right"></img>
            <p>One of the hottest subjects in <abbr>HTML5</abbr> is Canvas, but what exactly a Canvas is?</p>

            <blockquote>
            <p>
                Canvas is an extremely heavy-duty plain-woven fabric used for making sails, 
                tents, marquees, backpacks, and other items for which sturdiness is required. 
                It is also popularly used by artists as a painting surface, 
                typically stretched across a wooden frame.  ~ <a href="http://en.wikipedia.org/wiki/Canvas">Wikipedia</a> 
            </p>           
            </blockquote>
            
            <p>
                Perhaps that is not the answer that you are seeking. 
                In fact, the HTML5 Canvas is mimicking the physical canvas that is used by artist.
            </p>
            
            <p>    
                A canvas is just blank space with no color (not even white), and javascript being the 
                pencil and paint brush that going to turn a blank sheet into a piece of art.
            </p>
            
            <p>    
                Of course, the existence of canvas does not meant for just still image. Still image can be easily
                handled by decade-old "img" element. 
                With the infamous javascript as your paint brush, you can create 
                very promising animation or even interative games (here is a HTML5 Canvas showcase <a href="http://www.pirateslovedaisies.com/">Pirates love Daisies</a>)
            </p>
            
            <p>
                If you have created some great flash animation before and you hope to create a HTML5 version, there are tools 
                out there which can help you to accomplish that. 
            </p>
            
            <p>
            One of them are <a href="http://www.flash-to-html5.net"> Flash to HTML5</a>.
            </p>
            
            <p>
                HTML5 Canvas is a very big subject, it is big enough for you write a book with. It is not my
                intention to cover every single detail of Canvas in this tutorial. This is just a brief introduction
                of Canvas. Remember,it is just the begining.
            </p>
            
            <p>
                To create a canvas, the <abbr>HTML</abbr> code is simply
            </p>
            <code>
                &lt;canvas width="200" height="200"&gt;&lt;/canvas&gt;
            </code>
            <p>
                However, a borderless canvas is very much an invisible object. It takes up space but you don't see it.
                Let's add a border style and use a ID for 
                reference. The purpose of ID is to make it accessible using <abbr>DOM</abbr> (Document Object Model).
                Without reference of ID, javascript (in this case like your paint brush) will have no idea
                which canvas to paint.
            </p>
            <code>
                &lt;canvas id="Canvas1" width="200" height="200" style="border:solid 1px #000000"&gt;&lt;/canvas&gt;            
            </code>
            <div class="canvas_demo">
                <canvas id="Canvas1" width="200" height="200" style="border:solid 1px #000000">
                    <img src="images/skull.png" alt="No canvas support" title="No canvas support" />
                </canvas>
            </div>

            <p>
                If you see a skull with cross bones above instead of just a solid line square box, it means your web browser doesn't support Canvas.
                You may want to download a web browsers that work with Canvas as shown below.
            </p>
            
            <table class="browser">
                <thead>
                    <tr><th>Browsers</th><th>Basic Canvas Support</th></tr>
                </thead>
                <tbody>
                    <tr><td>IE 7</td><td>&#10003; (requires <a href="http://code.google.com/p/explorercanvas/">ExplorerCanvas</a>)</td></tr>
                    <tr><td>IE 9 Beta</td><td>&#10003; </td></tr>
                    <tr><td>Firefox 3.0</td><td>&#10003;</td></tr>
                    <tr><td>Safari 3.0</td><td>&#10003;</td></tr>
                    <tr><td>Chrome 3.0</td><td>&#10003;</td></tr>
                    <tr><td>Opera 10</td><td>&#10003;</td></tr>
                </tbody>
            </table>           
            
        </article>
<?php include("page_footer.php"); ?>    
