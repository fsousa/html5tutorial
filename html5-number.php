<?php $lesson=300; ?>
<?php include("page_header.php"); ?> 
    <article>
        <header>
            <h1><?php echo $lesson_arr[$current_id][1]?></h1>
        </header>
<script type="text/javascript"><!--
google_ad_client = "ca-pub-3162865608794255";
/* Html5 below title */
google_ad_slot = "6072872264";
google_ad_width = 728;
google_ad_height = 15;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>            
            <p>
                In HTML5, you can have a number input field as a spinner which you have up and down arrow at the right of the 
                textbox to increase or decrease the number value. Please see the spinner gallery as below
            </p>
            
            <img class="clipart" src="images/spinner_gallery.png" alt="Spinner Gallery" title="Spinner Gallery" />

            <p>
                As of writing, the web browsers that does the job in Windows are Google Chrome and Opera.
                
            </p>
                        
            <table class="browser">
                <thead>
                    <tr><th>Browsers</th><th>Render "Number" input as Spinner</th></tr>
                </thead>
                <tbody>
                    <tr><td>IE 9 Beta</td><td></td></tr>
                    <tr><td>Firefox 13</td><td></td></tr>
                    <tr><td>Safari 5</td><td>&#10003; (in Mac OS, but not Windows)</td></tr>
                    <tr><td>Chrome 8</td><td>&#10003;</td></tr>
                    <tr><td>Opera 11</td><td>&#10003;</td></tr>
                </tbody>
            </table>
                        
            <p>Let's have some real demo so you can touch it by your own self.</p>
            <label for="movie">How often you watch movie in week : </label><input id="movie" type="number" value="0"/>
            
            <code>
                &lt;label for="movie"&gt;How often you watch movie in week : &lt;/label&gt;&lt;input id="movie" type="number" value="0"/&gt;            
            </code>
            
            <p>
                If you are using Google Chrome 8, try input a non number value like "a", "B" ,"?", 
                Chrome won't allow you to do so, but it allows you to input "e"? Why is that so?
                because "e" can be a part of a floating point number. For example : 5.34 e+12
            </p>
            
            <p>
                On the other hand, Opera allows you do input anything as you like to it. 
            </p>
            
            <p>
                We shall explore some important attributes that we can have in Number Type (&lt;input type="number"&gt;) input.
            </p>
            
            <table class="attribute">
                <thead>
                    <tr>
                        <th>Attributes</th>
                        <th>Descriptions</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>value</td>
                        <td>Value is the default value of the input box when a page is first loaded. 
                        This is a common attribute for &lt;input&gt; element regardless which type you are
                        using.
                        </td>
                    </tr>                  
                    <tr>
                        <td>min</td>
                        <td>Obviously, the minimum value you of the number. I should have specified minimum value to 0 
                        for my demo up there as a negative number doesn't make sense for number of movie watched in a week.</td>
                    </tr>
                    <tr>
                        <td>max</td>
                        <td>Apprently, this represents the biggest number of the number input.</td>
                    </tr>
                    <tr>
                        <td>step</td>
                        <td>Step scale factor, default value is 1 if this attribute is not specified.</td>
                    </tr>
                </tbody>
            </table>

                    <p>
                        Let's have another demo, If you are software company and your software is sold 
                        by number of user license, minimum licenses you sell is 5, each increase is 5, maximum 
                        licenses per customer you can sell is 30. 
                        
                    </p>
                    <label for="user_lic">Number of user license : </label><input id="user_lic" type="number" min="5" max="30" step="5" value ="5"/>     
                    <code>
                        &lt;label for="user_lic"&gt;Number of user license : &lt;/label&gt;&lt;input id="user_lic" type="number" min="5" max="30" step="5" value ="5"/&gt;                    
                    </code>
                    
                    <p>
                        If you happen to use javascript to increase and decrease the value, here is the solution.
                    </p>
                    <ul>
                        <li>
                            spinner.stepUp(x) - x being the value you want to increase in the field.
                        </li>
                        <li>
                            spinner.stepDown(x) - x being the value you want to decrease in the field.
                        </li>
                        <li>
                            spinner.valueAsNumber - return value of input as floating point number instead of string.
                        </li>                                                
                    </ul>
                    
                    <p>
                        A spinner with relatively tiny up/down arrow might not be very finger friendly for touch screen application.
                        Both iPhone (iOS 4.2) and Android 2.2 web browsers render &lt;input type="number"&gt;
                        as normal textbox but give users a Number keypad for data input.
                    </p>
                    <p>
                        Screen of iPhone (iOS 4.2) on the left and HTC Desire (Android 2.2) on the right.
                    </p>
                    <img class="clipart" src="images/phone_number_field.png" alt="number type input in Android and iPhone" title="number type input in Android and iPhone" />
                        
        </article>
<?php include("page_footer.php"); ?>   