<?php $lesson=420; ?>
<?php include("page_header.php"); ?> 
    <article>
        <header>
            <h1><?php echo $lesson_arr[$current_id][1]?></h1>
        </header>
        <script type="text/javascript"><!--
google_ad_client = "ca-pub-3162865608794255";
/* Html5 below title */
google_ad_slot = "6072872264";
google_ad_width = 728;
google_ad_height = 15;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script> 
            <p>
                The "aside" element is a section that somehow related to main content, but it can be separate from that content.
                In this tutorial, I use "aside" element for tips related to the topic. What mentioned in the the yellow block
                provides additional information to reader, however, a reader can actually continue the tutorial without reading the tips.
            </p>
            <img alt="aside element" class="clipart" src="images/aside.png" />
            
            <code>
            &lt;article&gt;<br />
            &lt;p&gt;<br />
                As of writing, the only web browser completely support date time input is Opera.<br />
                In HTML5, it is the job of web browser to ensure user can only enter a valid date time string <br />
                into the input textbox.<br />
            &lt;/p&gt;<br />
            <br />
            <mark>&lt;aside&gt;</mark><br />
                Picking a date from Calendar is not the only way to input a date value even though it's the most popular implementation.<br />
                HTML5 specifications does not mention anything about displaying a calendar for date input.<br />
            <mark>&lt;/aside&gt;</mark>  <br />
            &lt;/article&gt;          
            </code>
            <p>
                The specifications of <abbr>W3C</abbr> says:
            </p>
            <p>
                The aside element represents a section of a page that consists of content that is 
                <em>tangentially</em> related to the content around the aside element, and which could be <em>considered separate </em>
                from that content. Such sections are often represented as sidebars in printed typography.
            </p>
            <p>
                The element can be used for typographical effects like pull quotes or sidebars, for advertising, 
                for groups of nav elements, and for other content that is considered separate from the main content of the page.            
            </p>
            
        </article>
<?php include("page_footer.php"); ?>   