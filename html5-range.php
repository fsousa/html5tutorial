<?php $lesson=290; ?>
<?php include("page_header.php"); ?> 
    <script>
        function printValue(sliderID, textbox) {
            var x = document.getElementById(textbox);
            var y = document.getElementById(sliderID);
            x.value = y.value;
        }

        window.onload = function() { printValue('slider1', 'rangeValue1'); printValue('slider2', 'rangeValue2'); printValue('slider3', 'rangeValue3'); printValue('slider4', 'rangeValue4'); }
    </script> 
    <article>
        <header>
            <h1><?php echo $lesson_arr[$current_id][1]?></h1>
        </header>
<script type="text/javascript"><!--
google_ad_client = "ca-pub-3162865608794255";
/* Html5 below title */
google_ad_slot = "6072872264";
google_ad_width = 728;
google_ad_height = 15;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>            
            <p>
                Slider control is a very intuitive user interface to set a number within a range. 
                A typical Slider usually can be found in color picker where we can drag the arrow left and right
                to pick the right RGB value.
            </p>
            
            <img class="clipart" alt="typical slider" title="Typical Slider control" src="images/slider_example.png"/>            
            
            <p>
                In order to create a Slider in a web page, we used to choose between javascript or flash as our solution.
                HTML5 is here to save thousands of bytes in your code. 
                With the new input type (&lt;input type="range"&gt;), Slider control will be
                native as Dropdown List (&lt;select&gt;)
            </p>
            
            <p>
                The code can be as simple as :-
            </p>
            
            <code>
                 &lt;input id="test" type="range"/&gt;
            </code>
            
            <p>
                As usual, the designer behind each web browser has their own taste in rendering user interface.
                Again, I would like to show you the gallery of Sliders in various browsers running in Windows.
            </p>
            
            <img class="clipart" alt="Slider Gallery" title="Slider Gallery" src="images/slider_gallery.png" />
            
            <p>    
                Browsers do not render &lt;input type="range"&gt; as slider will simply 
                render it as a normal textbox (&lt;input type="text"&gt;).
            </p>            
            <p>
                As of writing, web browsers that render &lt;input type="range"&gt; as slider as follows:-
            </p>
                        
            <table class="browser">
                <thead>
                    <tr><th>Browsers</th><th>Render Range input as Slider</th></tr>
                </thead>
                <tbody>
                    <tr><td>IE 9 Beta</td><td></td></tr>
                    <tr><td>Firefox 13</td><td></td></tr>
                    <tr><td>Safari 5</td><td>&#10003;</td></tr>
                    <tr><td>Chrome 8</td><td>&#10003;</td></tr>
                    <tr><td>Opera 11</td><td>&#10003;</td></tr>
                </tbody>
            </table>
            
            <p>
                Let's look at the demo of a slider. Well, I admit it is not sexy at all.
            </p>
            <div class="canvas_demo">
                <label for="rate">How much do you know about HTML5? (Rate 1 to 10) : </label><input id="test" type="range" />
            </div>
            
            <p>
                The Range type input has a few interesting attributes which you may want to learn.
            </p>
            
            <table class="attribute">
                <thead>
                    <tr>
                        <th>Attribute</th>
                        <th>Descriptions</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>value</td>
                        <td>
                            Value is a common attribute of "Input" element. 
                            The value can be any valid floating point number, not neccessary must be an integer number.
                            Default value is minimum value plus half of the maximum value.
                        </td>
                    </tr>
                    <tr>
                        <td>min</td>
                        <td>Minimum value of the range. Default minimum value is 0.</td>
                    </tr>
                    <tr>
                        <td>max</td>
                        <td>Maximum value of the range. Default maximum value is 100.</td>
                    </tr>
                    <tr>
                        <td>step</td>
                        <td>This is Step scale factor of the slider, default value is 1 and only allowing integer number
                        unless minimum value is non integer number.</td>
                    </tr>                    
                    <tr>
                        <td>list</td>
                        <td>List is the <a href="html5-datalist.php">DataList</a> in the past lesson.
                            Datalist can be incorporated into Range type input, however, 
                            none of the browser has implemented this feature as of writing.
                         </td>
                    </tr> 
                </tbody>
            </table>
            <p>
                It is always easier to understand with some hands on demos.
            </p>
            <p>
                Demo 1. Let's specify minimum value, maximum value and step factor.
            </p>

            <code>
            &lt;input id="slider1" type="range" min="100" max="500" step="10" /&gt;
            </code>
        
            <div class="canvas_demo">
            <p>
                Minimum = 100, Maximum = 500, Step = 10
            </p>    
                    <input id="slider1" type="range" min="100" max="500" step="10" onchange="printValue('slider1','rangeValue1')"/>
                    <input id="rangeValue1" type="text" size="2"/>
            </div>
            
            <aside>
                Don't be confused with the textbox that shows the number sitting beside the slider,
                It is not part of the Range type input but created only for the purpose of demo.
            </aside>
           
            <p>
                Demo 2. Set default value.
            </p>
            <code>
            &lt;input id="slider2" type ="range" min ="100" max="500" step ="50" value ="100"/&gt;
            </code>
        
            <div class="canvas_demo">
            <p>
                Minimum = -300, Maximum = 300, Step = 50, Value = 100
            </p>    
                    <input id="slider2" type="range" min="-300" max="300" step="50" value ="-300" onchange="printValue('slider2','rangeValue2')"/>
                    <input id="rangeValue2" type="text" size="2"/>
            </div>            

            <p>
                Demo 3. Let's see how it works in floating number.
            </p>
            <code>
            &lt;input id="slider3" type ="range" min ="-2.5" max="3.0" step ="0.1"/&gt;
            </code>
            
            <div class="canvas_demo">
            <p>
                Minimum = -2.5, Maximum = 3.0, Step = 0.1
            </p>    
                    <input id="slider3" type="range" min="-2.5" max="3.0" step="0.1" onchange="printValue('slider3','rangeValue3')"/>
                    <input id="rangeValue3" type="text" size="5" />
            </div>
        </article>
<?php include("page_footer.php"); ?>   