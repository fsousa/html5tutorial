﻿<?php $lesson=140; ?>
<?php include("page_header.php"); ?> 
    <article>
        <header>
            <h1><?php echo $lesson_arr[$current_id][1]?></h1>
        </header>
<script type="text/javascript"><!--
google_ad_client = "ca-pub-3162865608794255";
/* Html5 below title */
google_ad_slot = "6072872264";
google_ad_width = 728;
google_ad_height = 15;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>        
            <p>
                Let's have something fun to start with. We are now talking about web browser supporting 
                audio file in native, just like how &lt;img&gt; tag is supported since 1994. 
                <abbr>HTML5</abbr> is likely to put an end to audio plug-in such as Microsoft Windows Media player, Microsoft Silverlight, Apple QuickTime 
                and the infamous Adobe Flash.
            </p>
            <audio controls preload="none">
                <source src= "media/vincent.mp3" type="audio/mpeg" />
                <source src= "media/vincent.ogg" type="audio/ogg" />
            </audio>
  
            <p>
                If you don't see a audio player control in the dotted box above, your web browser probably don't support 
                the audio tag.
            </p>
            <img src="images/audio_gallery.png" class="clipart", title="Native Audio player controls" alt="Native Audio Player controls"/>
            <p>
                Above is a gallery of audio players by major web browsers. As you can see the size 
                of the player control varies from one to another, IE9 player is exceptionally large in compared to 
                Safari player. This can be a disaster to a web designer.
            </p>
            
            <p>
                If you are using Safari for Windows but without QuickTime, your Safari 
                is not going to support media tag (both &lt;audio&gt; and &lt;video&gt;) in "native".
                I suppose Safari is relying on QuickTime codec to play media file in the browser.
            </p>
            <h2>How to?</h2>
            <p>
                In order to make your web page plays music, the html code can be as simple as
            </p>
            <code>
                &lt;audio src="vincent.mp3" controls&gt;&lt;/audio&gt;
            </code>
            <p>
                Unfortunately, the most popular audio format MPEG3(.mp3) is not an Open standard, it is patent encumbered.
                That means, web browser needs to pay a sum of money in order to decode it, and that might not be 
                financially feasible for smaller company or organization. As you can see from table below, only 
                those big boys are rich enough to decode MP3. Firefox and Opera supports only Vorbis (.ogg) format
                which is an Open standard.
            </p>
            <p>
                On the other hand, the open standard Vorbis (*.ogg) is not supported by Safari and IE9. Hence, it is 
                always good to have both Mp3 and Ogg side to side available. 
            </p>                
                
            <table class="browser">
                <thead>
                    <tr>
                        <th>Browser</th>
                        <th>.mp3</th>
                        <th>.wav</th>
                        <th>.ogg</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Mozzila Firefox 3.6</td>
                        <td></td>
                        <td>&#10003;</td>
                        <td>&#10003;</td>
                    </tr>
                     <tr>
                        <td>Opera 10.63</td>
                        <td></td>
                        <td>&#10003;</td>
                        <td>&#10003;</td>
                    </tr>                   
                    <tr>
                        <td>Google Chrome 8.0</td>
                        <td>&#10003;</td>
                        <td>&#10003;</td>
                        <td>&#10003;</td>
                    </tr>
                    <tr>
                        <td>Apple Safari 5.0.3 (with QuickTime)</td>
                        <td>&#10003;</td>
                        <td>&#10003;</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Microsoft IE 9 Beta</td>
                        <td>&#10003;</td>
                        <td>&#10003;</td>
                        <td></td>
                    </tr>                                                                
                </tbody>
            </table>
            
            <p>
            Test how far your browser support audio tag using "<a href="http://www.happyworm.com/jquery/jplayer/HTML5.Audio.Support/" 
            title="HTML5 &lt;audio&gt; and Audio() Support Tester">HTML5 &lt;audio&gt; and Audio() Support Tester</a>".</p>                
            
            <p>What you can do is ...</p>
            <code>
                &lt;audio controls&gt;<br />
                &nbsp&nbsp&lt;source src="vincent.mp3" type="audio/mpeg"/&gt;<br />
                &nbsp&nbsp&lt;source src="vincent.ogg" type="audio/ogg"/&gt;<br />
                &lt;/audio&gt;
            </code>
            <p>Whether or not to provide the MIME type (type="audio/mpeg") to browser is optional. Most modern web browsers are 
            smart enough to determine the content type by itself. However, it is always good to be helpful to web browser, that
            makes your web browser works faster and happier.</p>    
            
            
            <p><h2>Attributes of &lt;audio&gt;</h2></p>
            
            <table class="attribute">
                <thead>
                    <tr>
                        <th id="att_name">Attribute</th>
                        <th id="att_value">Value</th>
                        <th id="att_desc">Description</th>
                    </tr>
                 </thead>
                <tbody>
                    <tr>
                        <td>controls</td>
                        <td>*Boolean attribute</td>
                        <td>You need this to make the native audio player appear. Otherwise, you would have 
                        to use DOM to control the audio element to play your music.
                        </td>
                    </tr>
                    <tr>
                        <td>autoplay</td>
                        <td>*Boolean attribute</td>
                        <td>If this guy exists, the browser will just play your song or your speech without asking permission from your visitor.</td>
                    </tr>
                    <tr>
                        <td>loop</td>
                        <td>*Boolean attribute</td>
                        <td>Keep repeating your music</td>
                    </tr> 
                    <tr>
                        <td>src</td>
                        <td>url</td>
                        <td>The URL of your audio file</td>
                    </tr> 
                    <tr>
                        <td>preload</td>
                        <td>none | metadata | auto</td>
                        <td>
                            This attribute was formerly known as "autobuffer" and it was an boolean attribute as "controls".<br /><br />
                            none - do not buffer audio file automatically.<br />
                            metadata - only buffer the metadata of audio file.<br />
                            auto - buffer audio file before it gets played.
                        </td>
                    </tr>                                                                                 
                </tbody>
            </table>

            <p>
                *Boolean attribute is an attribute that either present in the tag or not present. A Boolean attribute has just its name and no value.
                You can put it this way too, whatever value you assign to a boolean attribute means only one thing - TRUE.
            </p>
            <code>
                &lt;audio src="vincent.mp3" controls="true" loop="true" autoplay="true"&gt;&lt;/audio&gt; <br />
            </code>
            <p>(This is completely unneccessary!)</p>
                
            <code>    
                &lt;audio src="vincent.mp3" controls loop autoplay&gt;&lt;/audio&gt;<br />
            </code>
            <p>(This is it!)</p>
            
            <h2>How should we cater for less modern web browser?</h2>
            <p>
               <abbr>HTML5</abbr> is a evolution of the web, not a revolution that will totally destroy the past. Hence, let's
               move forward a little bit gracefully.
            </p>
            <p>
                In order to support web browser that doesn't understand what &lt;audio&gt; is about, such as IE8 and below, we shall 
                let them enjoy their good old day using &lt;object&gt; tag.
            </p>
            <code>
                &lt;audio controls&gt;<br />
                &nbsp&nbsp&lt;source src="vincent.mp3" type="audio/mpeg"/&gt;<br />
                &nbsp&nbsp&lt;source src="vincent.ogg" type="audio/ogg"/&gt;<br />
                &nbsp&nbsp&lt;object type="application/x-shockwave-flash" data="media/OriginalMusicPlayer.swf" width="225" height="86"&gt; <br />
                &nbsp&nbsp&nbsp&lt;param name="movie" value="media/OriginalMusicPlayer.swf"/&gt;<br />
                &nbsp&nbsp&nbsp&lt;param name="FlashVars" value="mediaPath=vincent.mp3" /&gt; <br />
                &nbsp&nbsp&lt;/object&gt; <br />                
                &lt;/audio&gt;
            </code>
            <aside>In my example above, I am using a flash mp3 player from <a href="http://www.premiumbeat.com/flash_music_players/original/single/">PremiumBeat.com</a></aside>            

            <p>If your browser doesn't suppport <abbr>HTML5</abbr> but you have got flash plug-in, you probably don't want to miss the fun. Here you go ...</p>
            <audio controls preload="none">
              <source src="media/vincent.mp3" type="audio/mpeg"/>
              <source src="media/vincent.ogg" type="audio/ogg"/>
              <object type="application/x-shockwave-flash" data="media/OriginalMusicPlayer.swf" width="225" height="86"> 
               <param name="movie" value="media/OriginalMusicPlayer.swf"/>
               <param name="FlashVars" value="mediaPath=media/vincent.mp3" /> 
              </object> 
              <a href="media/vincent.mp3">Download this lovely song and wish you all the best!</a>
            </audio>  
            <p>If the web browser support neither <abbr>HTML5</abbr> nor Flash, you might as well let the user download the audio file and wish them luck on 
            getting a player to play it.</p>                      
            <code>
                &lt;audio controls&gt;<br />
                &nbsp;&nbsp;&lt;source src="vincent.mp3" type="audio/mpeg"/&gt;<br />
                &nbsp;&nbsp;&lt;source src="vincent.ogg" type="audio/ogg"/&gt;<br />
                &nbsp;&nbsp;&lt;object type="application/x-shockwave-flash" data="media/OriginalMusicPlayer.swf" width="225" height="86"&gt; <br />
                &nbsp;&nbsp;&nbsp;&lt;param name="movie" value="media/OriginalMusicPlayer.swf"/&gt;<br />
                &nbsp;&nbsp;&nbsp;&lt;param name="FlashVars" value="mediaPath=vincent.mp3" /&gt; <br />
                &nbsp;&nbsp&lt;/object&gt; <br /> 
                &nbsp;&nbsp;&lt;a href="vincent.mp3"&gt;Download this lovely song and wish you all the best!&lt;/a&gt;<br />               
                &lt;/audio&gt;
            </code>
            <p>
                Instead of using the standard web browser audio player, you can write your own control, use your creativity and imagination, the sky is the limit.
            </p>
            <code>
                &lt;audio id="player" src="vincent.mp3"&gt;&lt;/audio&gt;<br /> 
                &lt;div&gt; <br />
                  &lt;button onclick="document.getElementById('player').play()"&gt;Play&lt;/button&gt; <br />
                  &lt;button onclick="document.getElementById('player').pause()"&gt;Pause&lt;/button&gt; <br />
                  &lt;button onclick="document.getElementById('player').volume += 0.1"&gt;Vol+ &lt;/button&gt; <br />
                  &lt;button onclick="document.getElementById('player').volume -= 0.1"&gt;Vol- &lt;/button&gt; <br />
                &lt;/div&gt;<br />            
            </code>            


        </article>
<?php include("page_footer.php"); ?>   
