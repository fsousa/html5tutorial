<?php $lesson=240; ?>
<?php include("page_header.php"); ?> 
    <article>
        <header>
            <h1><?php echo $lesson_arr[$current_id][1]?></h1>
        </header>
<script type="text/javascript"><!--
google_ad_client = "ca-pub-3162865608794255";
/* Html5 below title */
google_ad_slot = "6072872264";
google_ad_width = 728;
google_ad_height = 15;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>             
            <p>
                Autofocus is an boolean attribute of form field that make browser set focus on it when a page is loaded.
                If you still don't understand what that is, please go to <a href="http://www.google.com">Google</a> and 
                you notice you just type your search string without first click on the big textbox.
            </p>
            <p>
                This is because Google has set focus to that textbox automatically (I am not saying Google uses HTML5 "autofocus")
                and save you ONE mouse click.
            </p>
            
            <p>below is the code to do it, piece of cake.</p>
            <code>
                &lt;label for="name"&gt;Your name goes here :&lt;/label&gt;&lt;input id ="name" type="text" <mark>autofocus</mark>/&gt; &lt;br /&gt;<br />
                &lt;label for="mothername"&gt;Your dog's name goes here :&lt;/label&gt;&lt;input id ="mothername" type="text" /&gt;            
            </code>
            
            <p>"autofocus" is a boolean attribute, it is needless to set any value to it.</p>
            
            <p>As usual, not every web browser supports "autofocus"</p>
            <table class="browser">
                <thead>
                    <tr><th>Browsers</th><th>Autofocus Support</th></tr>
                </thead>
                <tbody>
                    <tr><td>IE 9 Beta</td><td></td></tr>
                    <tr><td>Firefox 4</td><td>&#10003;</td></tr>
                    <tr><td>Safari 4.0</td><td>&#10003;</td></tr>
                    <tr><td>Chrome 3.0</td><td>&#10003;</td></tr>
                    <tr><td>Opera 10</td><td>&#10003;</td></tr>
                </tbody>
            </table>
            
            <p>To implement a little bit more foolproof "autofocus", you will have to again use
            Javascript to check if a web browser supports "autofocus", if not, just set focus with Javascript. </p>      
            
            <p>
                Below is actually a demo as well as a test. If your web browser support Autofocus, 
                you will notice the cursor is blinking in the first textbox, if your web browser 
                does not support it, it will be set focus at the second input textbox.
            </p>
            <script>
                function testAttribute(element, attribute) {
                    var test = document.createElement(element);
                    if (attribute in test) {
                        return true;
                    }
                    else {
                        return false;
                    }
                }

                window.onload = function() {
                    if (!testAttribute('input', 'autofocus'))
                        document.getElementById('Text2').focus(); //for browser has no autofocus support, set focus to Text2.
                }
            </script>

            <div class="canvas_demo">
            <form>
                <label for="Text1">Support Autofocus    : </label><input id ="Text1" type="text" autofocus /> <br />
                <label for="Text2">No Autofocus Support : </label><input id ="Text2" type="text" />
            </form>
            </div>
            
            <code>
            &lt;script&gt;<br />
               function testAttribute(element, attribute) {<br />
            &nbsp;&nbsp;        var test = document.createElement(element);<br />
            &nbsp;&nbsp;        if (attribute in test) {<br />
            &nbsp;&nbsp;&nbsp;            return true;<br />
            &nbsp;        }<br />
                    else <br />
            &nbsp;            return false;<br />
                }<br />
                <br />
                window.onload = function() {<br />
            &nbsp;         if (!testAttribute('input', 'autofocus'))<br />
            &nbsp;             document.getElementById('Text2').focus(); <br />
            //for browser has no autofocus support, set focus to Text2.<br />
                }<br />
            &lt;/script&gt; 
            <br />
                &lt;label for="Text1"&gt;Support Autofocus :&lt;/label&gt;&lt;input id ="Text1" type="text" <mark>autofocus</mark>/&gt; &lt;br /&gt;<br />
                &lt;label for="Text2"&gt;No Autofocus Support :&lt;/label&gt;&lt;input id ="Text2" type="text" /&gt;            
           
            
            </code>        
        </article>
<?php include("page_footer.php"); ?>   