<?php $lesson=405; ?>
<?php include("page_header.php"); ?> 
    <article>
        <header>
            <h1><?php echo $lesson_arr[$current_id][1]?></h1>
        </header>
<script type="text/javascript"><!--
google_ad_client = "ca-pub-3162865608794255";
/* Html5 below title */
google_ad_slot = "6072872264";
google_ad_width = 728;
google_ad_height = 15;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>         
            <p> 
                 As what the name suggests, the new "nav" element is for Navigation. Yes, "nav" element is meant for
                 the area with collection of site navigational links.
            </p>
            <p>
                Navigational links of a website are mostly placed at the header and sidebar, sometimes, it can be in footer.
            </p>
            <p>
                However, Not all groups of links on a page need to be in a nav element - 
                only sections that consist of major navigation blocks are appropriate for the nav element. 
                In particular, it is common for footers to have a short list of links to various pages of a site, 
                such as the terms of service, the home page, and a copyright page. The footer element alone is sufficient for such cases, without a nav element.            
            </p>
            <img class="clipart" src="images/nav1.jpg" />
            <img class="clipart" src="images/nav2.jpg" />
            
            <p>
                Below is an example of how can we use nav element, it is usually nested with ul element (unordered list) 
                for navigational links.
            </p>
            
            <code>
                 &lt;nav&gt;<br />
                  &lt;ul&gt;<br />
                   &lt;li&gt;&lt;a href="/"&gt;Home&lt;/a&gt;&lt;/li&gt;<br />
                   &lt;li&gt;&lt;a href="/events"&gt;Current Events&lt;/a&gt;&lt;/li&gt;<br />
                   &lt;li&gt;&lt;a href="/contact"&gt;Contact us&lt;/a&gt;&lt;/li&gt;<br />
                  &lt;/ul&gt;<br />
                 &lt;/nav&gt;   
            </code>


        </article>
<?php include("page_footer.php"); ?>   
