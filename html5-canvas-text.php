<?php $lesson=210; ?>
<?php include("page_header.php"); ?> 
    <article>
        <header>
            <h1><?php echo $lesson_arr[$current_id][1]?></h1>
        </header>
<script type="text/javascript"><!--
google_ad_client = "ca-pub-3162865608794255";
/* Html5 below title */
google_ad_slot = "6072872264";
google_ad_width = 728;
google_ad_height = 15;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>             
            <h2>How to display text in canvas</h2>
            
            <div class="canvas_demo"><canvas id="c6" width="600" height = "200" style="border:solid 1px #000000;"></canvas>
                <div>
                    <button class="canvas_button" onclick="showFillText();return true;">Fill Text</button>
                    <button class="canvas_button" onclick="showStrokeText();return true;">Stroke Text</button>
                    <button class="canvas_button" onclick="Clear_text();return true;">Erase Everything</button>
                </div>
            </div>
            
            <script>
                var c6 = document.getElementById("c6");
                var c6_context = c6.getContext("2d");

                function showFillText() {
                    c6_context.fillStyle = '#f00';
                    c6_context.font = 'italic bold 30px sans-serif';
                    c6_context.textBaseline = 'bottom';
                    c6_context.fillText('HTML5 is cool!', 50, 100);
                }

                function showStrokeText() {
                    c6_context.strokeStyle = "#003300";
                    c6_context.font = '40px san-serif';
                    c6_context.textBaseline = 'bottom';
                    c6_context.strokeText('HTML5 is cool?', 300, 100);
                }

                function Clear_text() {
                    c6_context.clearRect(1, 1, 600, 300);
                }                  
            </script>
            
            <p>
            The magic words for printing text in canvas are "fillStyle","strokeStyle", "font","textBaseline" and lastly "fillText" and "strokeText".
            </p>
            
            <code>
            &lt;div&gt;&lt;canvas id="c6" width="600" height = "200" style="border:solid 1px #000000;"&gt;&lt;/canvas&gt;<br />
                &lt;div&gt;<br />
                    &lt;button onclick="showFillText();return true;"&gt;Fill Text&lt;/button&gt;<br />
                    &lt;button onclick="showStrokeText();return true;"&gt;Stroke Text&lt;/button&gt;<br />
                    &lt;button onclick="Clear_text();return true;"&gt;Erase Everything&lt;/button&gt;<br />
                &lt;/div&gt;<br />
            &lt;/div&gt;<br />
            <br />
            &lt;script&gt;<br />
                var c6 = document.getElementById("c6");<br />
                var c6_context = c6.getContext("2d");<br />
<br />
                function showFillText() {<br />
                    c6_context.<mark>fillStyle</mark> = '#f00';<br />
                    c6_context.<mark>font</mark> = 'italic bold 30px sans-serif';<br />
                    c6_context.<mark>textBaseline </mark>= 'bottom';<br />
                    c6_context.<mark>fillText('HTML5 is cool!', 50, 100)</mark>;<br />
                }<br />
<br />
                function showStrokeText() {<br />
                    c6_context.<mark>strokeStyle</mark> = "#003300";<br />
                    c6_context.<mark>font</mark> = '40px san-serif';<br />
                    c6_context.<mark>textBaseline</mark> = 'bottom';<br />
                    c6_context.<mark>strokeText('HTML5 is cool?', 300, 100)</mark>;<br />
                }<br />
<br />
                function Clear_text() {<br />
                    c6_context.clearRect(1, 1, 600, 300);<br />
                }                  <br />
            &lt;/script&gt;            
            
            </code>

            <table class="attribute">
                <thead>
                    <tr>
                        <th>Context method</th><th>Description</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>fillText(text,x,y)</td>
                        <td>
                            Print the text with solid color within. Text color is determined by
                            fillStyle().
                        </td>
                    </tr>
                    <tr>
                        <td>strokeText(text,x,y)</td>
                        <td>
                            Print the text with only color the outline of the text. Text color is set
                            by strokeStyle().
                        </td>
                    </tr>
                    <tr>
                        <td>strokeStyle</td>
                        <td>
                            CSS color for text that call strokeText
                        </td>
                    </tr>                                        
                    <tr>
                        <td>fillStyle</td>
                        <td>
                            CSS color for text that call fillText
                        </td>
                    </tr>
                    <tr>
                        <td>font</td>
                        <td>
                            CSS font style such as "bold, 10px, san-serif"
                        </td>
                    </tr>
                    <tr>
                        <td>textBaseline</td>
                        <td>
                            This is a little bit tricky to explain. We will need another demo.
                            The value for this propery can be "top", "hanging", "middle", "alphabetic", "ideographic" and "bottom". 
                            Default value is "alphabetic".
                        </td>
                    </tr>                                        
                </tbody>
            </table>
            <p>
                Below is a picture that I borrowed from <a href="http://www.whatwg.org/specs/web-apps/current-work/multipage/the-canvas-element.html#2dcontext">WHATWG</a>
                which is a perfect illustration for all kinds of text baselines.
                What you should observe is how a text is being placed in relation to those baselines.
            </p>
            <img src="images/baselines.png" class="clipart" alt="Illustration of text baseline" title ="Illustration of text baseline" />
            
            <p>
                Here I have drawn a gray line at y = 100 and I am going to place each word at y = 100 but using different "textBaseline".
            </p>
            
            <aside>As of writing (21 December 2010), Firefox (either 3.0+ or 4 Beta) has no support on baseline "hanging". </aside>
            
            <div class="canvas_demo"><canvas id="c7" width="600" height = "200" style="border:solid 1px #000000;"></canvas>
            </div>
            
            <script>
                var c7 = document.getElementById("c7");
                var c7_context = c7.getContext("2d");

                function draw_line() {
                    c7_context.moveTo(0, 100);
                    c7_context.lineTo(600, 100);
                    c7_context.strokeStyle = "grey";
                    c7_context.stroke();
                }

                function textTop() {
                    c7_context.font = '30px arial';
                    c7_context.fillStyle = "red";
                    c7_context.textBaseline = "top";
                    c7_context.fillText('Top', 5, 100);
                }

                function textBottom() {
                    c7_context.font = '30px arial';
                    c7_context.fillStyle = "green";
                    c7_context.textBaseline = "bottom";
                    c7_context.fillText('Bottom', 80, 100);
                }

                function textMiddle() {
                    c7_context.font = '30px arial';
                    c7_context.fillStyle = "olive";
                    c7_context.textBaseline = "middle";
                    c7_context.fillText('Middle', 200, 100);
                }

                function textAlphabetic() {
                    c7_context.font = '30px arial';
                    c7_context.fillStyle = "blue";
                    c7_context.textBaseline = "alphabetic";
                    c7_context.fillText('Alphabetic', 300, 100);
                }

                function textHanging() {
                    c7_context.font = '30px arial';
                    c7_context.fillStyle = "blueViolet";
                    c7_context.textBaseline = "hanging";
                    c7_context.fillText('Hanging', 450, 100);
                }
                window.onload = function() { draw_line(); textTop(); textBottom(); textMiddle(); textAlphabetic(); textHanging(); }
            </script>             
            <code>
                    c7_context.textBaseline = "top";<br />
                    c7_context.fillText('<mark>Top</mark>', 5, 100); <br />
                     <br />
                    c7_context.textBaseline = "bottom"; <br />
                    c7_context.fillText('<mark>Bottom'</mark>, 80, 100);  <br />
                     <br />
                    c7_context.textBaseline = "middle"; <br />
                    c7_context.fillText('<mark>Middle</mark>', 200, 100);  <br />
                     <br />
                    c7_context.textBaseline = "alphabetic"; <br />
                    c7_context.fillText('<mark>Alphabetic</mark>', 300, 100); <br />
                     <br />
                    c7_context.textBaseline = "hanging"; <br />
                    c7_context.fillText('<mark>Hanging</mark>', 400, 100);   <br />                                                                                      
            </code>
            
            <p>
                If you are planning to roll out something in Canvas and you wish you could support users running on
                 IE 8 or below, you can consider to use a free open source javascript library called <a href="http://code.google.com/p/explorercanvas/">ExplorerCanvas</a>.
                 Beware, work around is still a work around, there are some nonuniformity issues that might drive you mad.
            </p>
            
            <p>
                Again, this is just a brief introduction on <abbr>HTML5</abbr> Canvas, there are many more interesting features
                on <abbr>HTML5</abbr> Canvas out there to be explored. Have fun!
            </p>
        </article>
<?php include("page_footer.php"); ?>   