<?php 
  $non_lesson=true;
  $page_title="Advertise here";
?>
<?php include("page_header.php"); ?> 
    <article>
        <header>
            <h1>Advertise here !</h1>
        </header>

      <h2>Place your banner on our header at USD25 a month</h2>
      <p>
        Yes. Place your ad banner (730 x 90) on the header of this website at USD30 a month.
        Your banner appears in every page of this website.
      </p>
      <p>
        If you are interested, please send an email to web@html5tutorial.info
        
      </p>
      <p>
        Thank you!
        
      </p>
      <img class="clipart" src="images/html5-ad-position.png"/>
      
      <h2>About HTML5 Tutorial</h2>
      <p>
         This website ...
      </p>

      <ul>
        <li>
          attracts more than 20,000 <strong>Unique</strong> visitors a month
        </li>
        <li>
          generates close to 43,000 pageview a month.
        </li>
        <li>
          85%-90% of visitors come from Search engine
        </li>
        <li>Most visitors are <strong>web developers</strong> and <strong>web designers</strong> which are mainly from USA, India, Germany, UK, France and Brazil.</li>
        
      </ul>
      <img class="clipart" src="images/html5-traffic.png" alt="Web traffic"/>
             
        </article>
<?php include("page_footer.php"); ?>   