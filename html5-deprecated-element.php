<?php $lesson=430; ?>
<?php include("page_header.php"); ?> 
    <article>
        <header>
            <h1><?php echo $lesson_arr[$current_id][1]?></h1>
        </header>
<script type="text/javascript"><!--
google_ad_client = "ca-pub-3162865608794255";
/* Html5 below title */
google_ad_slot = "6072872264";
google_ad_width = 728;
google_ad_height = 15;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script> 
            <p>
                After the introduction of many new elements and attributes in the past lessons, it is time to talk about what you 
                should be avoiding in HTML5.
            </p>
            
            <p> 
                Ever since CSS was invented, web developers have been always told to segregate design from content. Let's CSS 
                take care of the styles and HTML take care the structure and content of your page.
            </p>
            <p>
                In <abbr>HTML5</abbr>, The life of the following elements have come to an end. 
                They have no role to play in HTML5 because CSS has grab it all.
            </p>

            <ul>
                <li>&lt;basefont&gt;</li>
                <li>&lt;big&gt;</li>
                <li>&lt;center&gt;</li>
                <li>&lt;font&gt;</li>
                <li>&lt;strike&gt;</li>
                <li>&lt;tt&gt;</li>
                <li>&lt;u&gt;</li>
            </ul>

            <p>
                The following elements has nothing to do with styling but they are simply a bad guys in HTML coding because
                they have been blamed of "damages usability and accessibility"
            </p>

            <ul>
                <li>&lt;frame&gt;</li>
                <li>&lt;frameset&gt;</li>
                <li>&lt;noframes&gt;</li>        
            </ul>


            <p>
                The following elements are not included in HTML5 because they have not been used often, created confusion, 
                or their function can be handled by other elements:
            </p>
            
            <ul>
                <li>&lt;acronym&gt; is gone, use &lt;abbr&gt; instead for abbreviations.</li>
                <li>&lt;applet&gt; has been obsoleted in favor of object.</li>
                <li>&lt;isindex&gt; usage can be replaced by usage of form controls.</li>
                <li>&lt;dir&gt; has been obsoleted in favor of &lt;ul&gt;.</li>
                <li>Finally the &lt;noscript&gt; element is only conforming in the HTML syntax. It is not included in the XML syntax as its usage relies on an HTML parser.  </li>
            </ul>
            
            <p>
                If you still insist on using these element, you web page will not blow, and yet, web browser will still be supporting it.
                So, the cost of doing is, your page will not pass in <abbr>HTML5</abbr> validation.
            </p>
        </article>
<?php include("page_footer.php"); ?>   