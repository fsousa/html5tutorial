﻿<?php
    $tutarray = array(
    100,110,120,130,140,150,160,170,180,190,
    200,210,220,230,240,250,260,270,280,290,
    300,310,320,330,340,350,360,370,380,390,
    400,405,410,420,430,440);
    
    $lesson_arr = array(
array(  1 => "Welcome", 2 => "index.php", 3=> "This is a HTML5 Tutorial site"),    
array(  1 => "Why is HTML5 important?", 2 => "html5-is-important.php", 3=> "Both Web browser vendors and web designer decide if HTML5 going to be significant"),
array(  1 => "The DocType", 2 => "doctype.php" ,3 => "DOCTYPE of HTML version 5 or HTML5"),
array(  1 => "HTML Header stuff", 2 => "html5-header-stuff.php", 3=> "HTML5 cut short some of the unneccessary redundant information in HTML header"),
array(  1 => "Audio", 2 => "html5-audio.php", 3=> "You do not need flash plugin for playing audio in your web page" ),
array(  1 => "Video", 2 => "html5-video.php", 3=> "You do not need flash plugin for playing video in your web page"),
array ( 1 => "Canvas", 2 => "html5-canvas.php", 3=> "Use javascript to create art in HTML5 Canvas"),
array ( 1 => "Canvas : Rectangle",2=> "html5-canvas-rect.php", 3 => "How to draw rectangle in Canvas",4=>"level2"),
array ( 1 => "Canvas : Shadow",2=> "html5-canvas-shadow.php", 3 => "How to draw shadow in Canvas",4=>"level2"),
array ( 1 => "Canvas : Path" ,2=> "html5-canvas-path.php", 3 => "How to draw path in Canvas",4=>"level2"),
array ( 1 => "Canvas : Image",2=> "html5-canvas-image.php", 3 => "How to display image file in Canvas",4=>"level2"),
array ( 1 => "Canvas : Text",2=> "html5-canvas-text.php", 3 => "How to show text in Canvas",4=>"level2"),
array ( 1 => "Web form 2.0", 2 => "html5-webform2.php", 3 => "Webform 2.0 is now in HTML5, rich HTML native control set"),
array ( 1 => "Input Attr : Placeholder", 2 => "html5-placeholder.php", 3=> "I will show you how to make placeholder textbox without javaScript",4=>"level2"),
array ( 1 => "Input Attr : Autofocus", 2 => "html5-autofocus.php", 3=> "Make a textbox to be autofocus after page loading without JavaScript",4=>"level2"),
array ( 1 => "Input Attr : Required field", 2 => "html5-required.php", 3=> "How to create a compulsory / mandatory field with HTML5",4=>"level2"),
array ( 1 => "Input Attr : DataList", 2 => "html5-datalist.php", 3=> "Cross breed textbox and select dropdown, you now have datalist",4=>"level2"),
array ( 1 => "Input Type : Search", 2 => "html5-search.php", 3=> "Create a search box in HTML5 which is essential for any content rich website",4=>"level2"),
array ( 1 => "Input Type : Email,URL,Phone", 2 => "html5-contact.php", 3=> "HTML5 let you specify specifically telephone, email, web url in your contact detail. Let's make it input friendly for mobile device",4=>"level2"),
array ( 1 => "Input Type : Range", 2 => "html5-range.php", 3=> "This is an input control that allow user to pick a value from a given range",4=>"level2"),
array ( 1 => "Input Type : Number", 2 => "html5-number.php", 3=> "Create a number input box that allows only number",4=>"level2"),
array ( 1 => "Input Type : Date",2 =>"html5-date.php", 3=> "A web browser native calendar date time picker control",4=>"level2"),
array ( 1 => "Input Type : Color",2 =>"html5-color.php", 3=> "Color picker make color picking fun and painless",4=>"level2"),
array ( 1 => "Towards Semantic Web", 2 => "html5-semantic.php", 3=> "Let's march towards semantic web"),
array ( 1 => "Semantic &lt;mark&gt;", 2 => "html5-mark.php", 3=> "To mark part of text that is important",4=>"level2"),
array ( 1 => "Semantic &lt;time&gt;", 2 => "html5-time.php", 3=> "Create a machine readable time value in your text content",4=>"level2"),
array ( 1 => "Semantic &lt;meter&gt;", 2 => "html5-meter.php", 3=> "The meter element represents a scalar measurement within a known range",4=>"level2"),
array ( 1 => "Semantic &lt;progress&gt;", 2 => "html5-progress.php", 3=> "The progress element represents either an indeterminate or determinate progress of task",4=>"level2"),
array ( 1 => "Semantic &lt;section&gt;", 2 => "html5-section.php", 3=> "The section element represents a generic section of a document or application. A section, in this context, is a thematic grouping of content, typically with a heading.",4=>"level2"),
array ( 1 => "Semantic &lt;header&gt;", 2 => "html5-header.php", 3=> "The header element represents a group of introductory or navigational aids",4=>"level2"),
array ( 1 => "Semantic &lt;footer&gt;", 2 => "html5-footer.php", 3=> "A footer typically contains information about its section such as who wrote it, links to related documents, copyright data, and the like.",4=>"level2"),
array ( 1 => "Semantic &lt;nav&gt;", 2 => "html5-nav.php", 3=> "The nav element represents a section of a page that links to other pages or to parts within the page: a section with navigation links.",4=>"level2"),
array ( 1 => "Semantic &lt;article&gt;", 2 => "html5-article.php", 3=> "The article element represents a self-contained composition in a document, page, application, or site and that is, in principle, independently distributable or reusable",4=>"level2"),
array ( 1 => "Semantic &lt;aside&gt;", 2 => "html5-aside.php", 3=> "aside element represents a section of a page that consists of content that is tangentially related to the content around the aside element, and which could be considered separate from that content. Such sections are often represented as sidebars in printed typography..",4=>"level2"),
array ( 1 => "Elements you shouldn't be using", 2 => "html5-deprecated-element.php", 3=> "The elements in this section are not to be used by web designer. Browsers will still have to support them and various sections in HTML5 define how. E.g. the obsolete isindex element is handled by the parser section."),
array ( 1 => "Attributes you shouldn't be using", 2 => "html5-obsolete-attribute.php", 3=> "Some attributes from HTML4 are no longer allowed in HTML5. The specification defines how user agents should process them in legacy documents, but authors must not use them and they will not validate."),
    
                             
    );
?>