<?php $lesson=330; ?>
<?php include("page_header.php"); ?> 
    <article>
        <header>
            <h1><?php echo $lesson_arr[$current_id][1]?></h1>
        </header>
<script type="text/javascript"><!--
google_ad_client = "ca-pub-3162865608794255";
/* Html5 below title */
google_ad_slot = "6072872264";
google_ad_width = 728;
google_ad_height = 15;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script> 
            <p> 
                Once upon a time (it was 1990), a man named Tim Berners-Lee gave birth to the World Wide Web that
                we all love today.
            </p>
            <p>    
                Don't get confused, he did not invent the Internet, but HTML and HTTP Protocol. Years later,
                he founded WWW Consortium aka W3C, an organization that 
                overseeing the development of the web and maintaining standard of the HTML and some other related technologies.
                He is still the director of W3C until today.
            </p>
            
            <p>
                Most great people are driven by their dream, Sir Tim Berners-Lee is no exception. He has a great dream too.
                Here you go.
            </p>
            <blockquote>
            <p>
                I have a dream for the Web... and it has two parts.
            </p>
            <p>
                In the first part, the Web becomes a much more powerful means for collaboration between people. 
                I have always imagined the information space as something to which everyone has immediate and intuitive access, 
                and not just to browse, but to create. 
                The initial WorldWideWeb program opened with an almost blank page, ready for the jottings of the user. 
                Robert Cailliau and I had a great time with it, not because we were looking for a lot of stuff, 
                but because we were writing and sharing our ideas. 
                Furthermore, the dream of people-to-people communication through shared knowledge must be possible for 
                groups of all sizes, interacting electronically with as much ease as they do now in person.
            </p>
            <p>    
                In the second part of the dream, collaborations extend to computers. 
                Machines become capable of analyzing all the data on the Web -- the content, 
                links, and transactions between people and computers. 
                A <em>"Semantic Web"</em>, which should make this possible, has yet to emerge, but when it does, 
                the day-to-day mechanisms of trade, bureaucracy, and our daily lives will be handled 
                by machines talking to machines, leaving humans to provide the inspiration and intuition. 
                The intelligent "agents" people have touted for ages will finally materialize. 
                This machine-understandable Web will come about through the implementation of 
                a series of technical advances and social agreements that are now beginning 
             </p>
             <p>   
                Once the two-part dream is reached, the Web will be a place where the whim of a 
                human being and the reasoning of a machine coexist in an ideal, powerful mixture.
                Realizing the dreeam will require a lot of nitty-gritty work. The Web is far from "done." 
                It is in only a jumbled state of construction, and no matter how grand the dream, 
                it has to be engineered piece by piece, with many of the pieces far from glamorous.            
            </p>
            </blockquote>
            
            <p>
                In short, the objective of Semantic web is making the Web understandable not just by human but also machines.
                (not machine with artificial intelligence) With that, information exchange and sharing can be much more efficient.
            </p>
            
            <p>
                HTML5 introduces a handful of new inline elements (such as span, strong, abbr) to the HTML family,
                in fact, it is no longer called inline element but text-level semantics.
            </p>
            
            <p>New Text-level semantics</p>
            <ul>
                <li>&lt;<a href="html5-mark.php">mark</a>&gt;</li>
                <li>&lt;<a href="html5-time.php">time</a>&gt;</li>
                <li>&lt;<a href="html5-meter.php">meter</a>&gt;</li>
                <li>&lt;<a href="html5-progress.php">progress</a>&gt;</li>
            </ul>
            
            <p>
                Besides text-level elements, there are series of blocking elements for content sectioning.
                With the addition of these new elements, it is aim to make sections of a web page more meaningful rather than
                overwhelmed with "div"s of various classes such as &lt;div class="header"&gt;....&lt;div class="footer"&gt; or &lt;div class="article"&gt;
            </p>
            
            <p>New Blocking Elements</p>
            <ul>
                <li>&lt;<a href="html5-section.php">section</a>&gt;</li>
                <li>&lt;<a href="html5-header.php">header</a>&gt;</li>
                <li>&lt;<a href="html5-footer.php">footer</a>&gt;</li>
                <li>&lt;<a href="html5-nav.php">nav</a>&gt;</li>
                <li>&lt;<a href="html5-article.php">article</a>&gt;</li>
                <li>&lt;<a href="html5-aside.php">aside</a>&gt;</li>                
            </ul>
            
        </article>
<?php include("page_footer.php"); ?>  