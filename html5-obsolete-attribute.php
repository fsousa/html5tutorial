<?php $lesson=440; ?>
<?php include("page_header.php"); ?> 
    <article>
        <header>
            <h1><?php echo $lesson_arr[$current_id][1]?></h1>
        </header>
        <script type="text/javascript"><!--
google_ad_client = "ca-pub-3162865608794255";
/* Html5 below title */
google_ad_slot = "6072872264";
google_ad_width = 728;
google_ad_height = 15;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script> 
            <p>
                In HTML5, none of the attribute should be used for styling purpose. Rule of thumb is, styling should be
                taken care by CSS.
            </p>
            
            <p>
                What I meant for "styling" is such as font size, color, border, font family, whatever is used for presentational purpose.
            </p>
        
            <p>
                In the table below, you should be avoid using attributes below in the elements listed in following table.
            </p>
            
            <table class="attribute">
                <thead>
                    <tr>
                        <th>Attributes</th>
                        <th>Elements</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>align</td>
                        <td>caption, iframe, img, input, object, legend, table, hr, div, h1, h2, h3, h4, h5, h6, p, col, colgroup, tbody, td, tfoot, th, thead and tr.</td>
                    </tr>

                    <tr>
                        <td>alink, link, text and vlink</td>
                        <td>body</td>
                    </tr>
                    
                    <tr>
                        <td>background</td>
                        <td>body</td>
                    </tr>
                    
                    <tr>
                        <td>bgcolor</td>
                        <td>table, tr, td, th and body</td>
                    </tr>

                    <tr>
                        <td>border</td>
                        <td>table and object</td>
                    </tr>

                    <tr>
                        <td>cellpadding and cellspacing</td>
                        <td>table</td>
                    </tr>
                    
                    <tr>
                        <td>char and charoff</td>
                        <td>col, colgroup, tbody, td, tfoot, th, thead and tr</td>
                    </tr>
                    
                    <tr>
                        <td>clear</td>
                        <td>br</td>
                    </tr>   

                    <tr>
                        <td>compact</td>
                        <td>dl, menu, ol and ul</td>
                    </tr>

                    <tr>
                        <td>frame</td>
                        <td>table</td>
                    </tr>
                    
                    <tr>
                        <td>frameborder</td>
                        <td>iframe</td>
                    </tr>
                    
                    <tr>
                        <td>height</td>
                        <td>td and th</td>
                    </tr>

                    <tr>
                        <td>hspace and vspace</td>
                        <td>img and object</td>
                    </tr>

                    <tr>
                        <td>marginheight and marginwidth</td>
                        <td>iframe</td>
                    </tr>
                    
                    <tr>
                        <td>noshade</td>
                        <td>hr</td>
                    </tr>
                    
                    <tr>
                        <td>nowrap</td>
                        <td>td and th</td>
                    </tr>

                    <tr>
                        <td>rules</td>
                        <td>table</td>
                    </tr>
                    
                    <tr>
                        <td>scrolling</td>
                        <td>iframe</td>
                    </tr>

                    <tr>
                        <td>size</td>
                        <td>hr</td>
                    </tr>

                    <tr>
                        <td>type</td>
                        <td>li, ol and ul</td>
                    </tr>
                    
                    <tr>
                        <td>valign</td>
                        <td>col, colgroup, tbody, td, tfoot, th, thead and tr</td>
                    </tr>
                    
                    <tr>
                        <td>width</td>
                        <td>hr, table, td, th, col, colgroup and pre</td>
                    </tr>                                                                                                                            
                </tbody>
            </table>
        </article>
<?php include("page_footer.php"); ?>   