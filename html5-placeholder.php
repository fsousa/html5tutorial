<?php $lesson=230; ?>
<?php include("page_header.php"); ?> 
    <article>
        <header>
            <h1><?php echo $lesson_arr[$current_id][1]?></h1>
        </header>
<script type="text/javascript"><!--
google_ad_client = "ca-pub-3162865608794255";
/* Html5 below title */
google_ad_slot = "6072872264";
google_ad_width = 728;
google_ad_height = 15;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script> 
            
            <p>
                A placeholder is a textbox that hold a text in lighter shade when there is no value and not focused.
                
            </p>
            <p>
                Below is how a placeholder should look like if no value is entered and textbox is not focus. 
                (What you see is just picture, not real placeholder, don't try to click it)
            </p>
            <img  alt="placeholder without value" 
            title ="placeholder without value" src="images/placeholder1.jpg" />
            <p>
                Once the textbox gets focus, the text goes off and you shall input your own text.
            </p>
            <img  alt="placeholder without value" 
            title ="placeholder without value" src="images/placeholder2.jpg" />
            
            <p>Without going further, let's have a simple demo</p>
            
            <div class="canvas_demo">
               <label for="first_name">First Name</label> : <input id="first_name" placeholder="First name goes here">
            </div>
            
            <p>..and, here is the code. Javascript and CSS is no longer needed to handle the appearance of the text.</p>
            <code>
                &lt;label for="first_name"&gt;First Name&lt;/label&gt; : &lt;input id="first_name" placeholder="First name goes here"&gt;
            </code>
            <p>
               Let's see what which browsers support placeholder.
            </p>
            
            <table class="browser">
                <thead>
                    <tr><th>Browsers</th><th>Placeholder Support</th></tr>
                </thead>
                <tbody>
                    <tr><td>IE 9 Beta</td><td></td></tr>
                    <tr><td>Firefox 3.7</td><td>&#10003;</td></tr>
                    <tr><td>Safari 4.0</td><td>&#10003;</td></tr>
                    <tr><td>Chrome 4.0</td><td>&#10003;</td></tr>
                    <tr><td>Opera 11</td><td>&#10003;</td></tr>
                </tbody>
            </table>  
            
            <p>If placeholder is just a nice-to-have feature in your web page, you probably will not bother about
            supporting browsers other than above.
            </p>
            <p>
                Otherwise, you would have to go extra mile. First, use Javascript to check if the browser support Placeholder,
                for browsers that does not support Placeholder, use Javascript + CSS to create a Placeholder on your own.
               
            </p>
            
            <code>
            &lt;label for="demo"&gt;Placeholder demo&lt;/label&gt; : &lt;input id ="demo" placeholder="<mark>Support Placeholder</mark>" /&gt;<br />
            <br />
            
            &lt;script&gt;<br />
            function <mark>testAttribute</mark>(element, attribute)<br /> 
            {<br />
            &nbsp;&nbsp;var test = document.createElement(element);<br />
            &nbsp;&nbsp;if (attribute in test) <br />
            &nbsp;&nbsp;&nbsp;            return true;<br />
            &nbsp;&nbsp;else <br />
            &nbsp;&nbsp;&nbsp;            return false;<br />
                }<br />
           <br />
                if (!testAttribute("input", "placeholder")) <br />
                {<br />
                &nbsp;    window.onload = function() <br />
                &nbsp;    {<br />
                &nbsp;&nbsp;&nbsp;        var demo = document.getElementById("demo");<br />
                &nbsp;&nbsp;&nbsp;        var text_content = "<mark>No Placeholder support</mark>";<br />
<br />
                &nbsp;&nbsp;&nbsp;        demo.style.color = "gray";<br />
                &nbsp;&nbsp;&nbsp;        demo.value = text_content;<br />
<br />
                &nbsp;&nbsp;&nbsp;        demo.onfocus = function() {<br />
                &nbsp;&nbsp;&nbsp;            if (this.style.color == "gray")<br />
                &nbsp;&nbsp;&nbsp;            { this.value = ""; this.style.color = "black" }<br />
                &nbsp;&nbsp;&nbsp;        }<br />
<br />
                &nbsp;&nbsp;&nbsp;        demo.onblur = function() {<br />
                &nbsp;&nbsp;&nbsp;            if (this.value == "")<br />
                &nbsp;&nbsp;&nbsp;            { this.style.color = "gray"; this.value = text_content; }<br />
                &nbsp;&nbsp;&nbsp;        }<br />
                &nbsp;    }                  <br />
                }<br />
            &lt;/script&gt;        <br />
                <br />
            
            </code>
            
            <p>
                "testAttribute" function above is to test if an attribute of an element is supported by a web browser.
                In this case, we want to know if your web browser supports "Placeholder" attribute of "Input" element.
            </p>
            <p>
                If "Placeholder is not supported, I make the browser to run some Javascript code to do create something as a Placeholder, 
                you can use your own Javascript instead of mine, (my code is created for showcase and might not be suitable for production)
            </p>
                      
            <div class="canvas_demo">
                <label for="demo">Placeholder demo</label> : <input id ="demo" placeholder="Support Placeholder" />
            </div>
              
            <script>
                function testAttribute(element, attribute) {
                    var test = document.createElement(element);
                    if (attribute in test) {
                        return true;
                    }
                    else {
                        return false;
                    }
                }
           
                if (!testAttribute("input", "placeholder")) 
                {
                    window.onload = function() 
                    {
                        var demo = document.getElementById("demo");
                        var text_content = "No Placeholder support";

                        demo.style.color = "gray";
                        demo.value = text_content;

                        demo.onfocus = function() {
                            if (this.style.color == "gray")
                            { this.value = ""; this.style.color = "black" }
                        }

                        demo.onblur = function() {
                            if (this.value == "")
                            { this.style.color = "gray"; this.value = text_content; }
                        }
                    }                  
                }
            </script>        
                
            <p>
                If you are using a web browser that supports "Placeholder", 
                you should be able to see "Support Placeholder" in the textbox. 
                Conversely, you will see "No Placeholder Support" if you are using
                web browser such as Internet Explorer 8.
            </p>


        </article>
<?php include("page_footer.php"); ?>   