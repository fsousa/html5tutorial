<?php $lesson=270; ?>
<?php include("page_header.php"); ?> 
    <article>
        <header>
            <h1><?php echo $lesson_arr[$current_id][1]?></h1>
        </header>
<script type="text/javascript"><!--
google_ad_client = "ca-pub-3162865608794255";
/* Html5 below title */
google_ad_slot = "6072872264";
google_ad_width = 728;
google_ad_height = 15;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>             
            <p>
                The information in the web is growing every single second. Since Google redefined Search in the late 90's,
                finding information through organized directory is dead. Today, we can spot a "search box" in almost all content 
                rich websites, it is even popular in personal blog.
            </p>
            
            <p>
                In HTML5, we can define a textbox as search box instead of a normal textbox.
                What you can actually do for your search box is this:-
            </p>
            
            <code>
            &lt;input id="mysearch" <mark>type="search"</mark> /&gt;
            </code>
            
            <p>
                By doing so, don't expect web browser to automatically blend it with a search engine. 
                It is almost like a normal textbox &lt;input type="text"&gt;, but browser might tweak it 
                slightly to make it cuter.
            </p>
            
            <p>
                As of writing, only Apple Safari (tested with Safari 5) and Google Chrome (tested with Google Chrome 8) 
                does that. 
            </p>
            
            <table class="browser">
                <thead>
                    <tr><th>Browsers</th><th>Tweak search box?</th></tr>
                </thead>
                <tbody>
                    <tr><td>IE 9 Beta</td><td></td></tr>
                    <tr><td>Firefox 13</td><td></td></tr>
                    <tr><td>Safari 5</td><td>&#10003;</td></tr>
                    <tr><td>Chrome 8</td><td>&#10003;</td></tr>
                    <tr><td>Opera 9</td><td></td></tr>
                </tbody>
            </table>
            
            <p>Here is a simple demo.</p>
            <p>
            <label for="mysearch">search : </label><input id="mysearch" type="search" />
            </p>
            <img class="clipart" src="images/input_type_search.png" alt="A search box" title="A search box in HTML5" />
            
            <p>
                As you may notice, there is a blue "cross" sign appears in the textbox when you input something in
                the search box, when you click on the "cross", your input string will be clear and you can start 
                to type a new string.
            </p>

            
            <p>
                You can add more input attribute such as "Placeholder" or "Autofocus". Below is a demo 
                of a search box with placeholder attribute.
            </p>
            <label for="mysearch2">Enter your search string here : </label><input id="mysearch2" type="search" placeholder="search" />
            
            <p>
                Here is the code behind it.
            </p>
            <code>
                &lt;label for="mysearch2"&gt;Enter your search string here : &lt;/label&gt;
                <br />
                &lt;input id="mysearch2" type="search" placeholder="search"&gt;            
            </code>
        </article>
<?php include("page_footer.php"); ?>   