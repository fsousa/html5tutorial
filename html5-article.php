<?php $lesson=410; ?>
<?php include("page_header.php"); ?> 
    <article>
        <header>
            <h1><?php echo $lesson_arr[$current_id][1]?></h1>
        </header>
<script type="text/javascript"><!--
google_ad_client = "ca-pub-3162865608794255";
/* Html5 below title */
google_ad_slot = "6072872264";
google_ad_width = 728;
google_ad_height = 15;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script> 
            <p> 
                The web today contains a ocean of news articles and blog entries. That gives W3C
                a good reason to define an element for article instead of &lt;div class="article"&gt;. 
            </p>
            
            <p>
                The "article" element is meant for article of a web document. It is very common to have more than
                one articles in one web page.  
            </p>
            
            <p>
                What you can see below is a web page that contains more than one article
                in a page. However, when you click title, you 
                will be brought to a page with one (1) article.
            </p>
            
            <img class="clipart" src="images/article.png" alt="can be article element"/>
            
            <p>
                We should use article for content that we think it can be distributable. Just like news or blog entry
                can we can share in RSS feed.
            </p>
            
            <p>
                "article" element can be nested in another "article" element. 
                A typical example is user-submitted comments of a blog entry.
                
                the inner article elements represent articles that are 
                in principle related to the contents of the outer article. 
                For instance, a blog entry on a site that accepts user comments could represent the 
                comments as another article elements nested within the article element for the blog entry.                
            </p>
            
            <p>
                An article element doesn't just mean article content. You can have <a href="html5-header.php">
                    header</a> and <a href="html5-footer.php">footer</a> element in an article. In fact, it is 
                very common to have <a href="html5-header.php">
                    header</a> as each article should have a title.
            </p>
            <code>
            &lt;body&gt;<br />
            &lt;h1&gt;My blog&lt;/h1&gt;<br />
                <mark>&lt;article&gt;</mark><br />
                <br />
                 <mark>&lt;header&gt;</mark><br />
                  &lt;h1&gt;The Very First Rule of Life&lt;/h1&gt;<br />
                  &lt;p&gt;&lt;time pubdate datetime="2009-10-09T14:28-08:00"&gt;&lt;/time&gt;&lt;/p&gt;<br />
                 <mark>&lt;/header&gt;</mark><br />
                 <br />
                 &lt;p&gt;If there's a microphone anywhere near you, assume it's hot and<br />
                 sending whatever you're saying to the world. Seriously.&lt;/p&gt;<br />
                 &lt;p&gt;...&lt;/p&gt;<br />
                 <br />
                 <mark>&lt;footer&gt;</mark><br />
                  &lt;a href="?comments=1"&gt;Show comments...&lt;/a&gt;<br />
                 <mark>&lt;/footer&gt;</mark><br />
                 <br />
                <mark>&lt;/article&gt;</mark><br />
            &lt;body&gt;<br /><br />                            
            </code>
            
            <p>
                In <abbr>HTML5</abbr>, It is perfectly okay to have h1 to h6 in an article element. As article element is a sectioning 
                content, which means, it will be a subsection of its ancestor. 
           </p>
           <p>     
                In our case, the article element is the subsection 
                of the body element. Hence, it can have its own outline. With such, it is easier to maintain our web document, because
                you do not need to think about the outline outside of the article element.
            </p>
            <p></p>
        </article>
<?php include("page_footer.php"); ?>   