<?php $lesson=360; ?>
<?php include("page_header.php"); ?> 
    <article>
        <header>
            <h1><?php echo $lesson_arr[$current_id][1]?></h1>
        </header>
<script type="text/javascript"><!--
google_ad_client = "ca-pub-3162865608794255";
/* Html5 below title */
google_ad_slot = "6072872264";
google_ad_width = 728;
google_ad_height = 15;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>         
            <p>
                "Meter" is a new element in HTML5 which represenet value of a known range as a gauge.
                The keyword here is "known range". That means, you are only allowed to use it when you are
                clearly aware of its minimum value and maximum value.
            </p>
            <p>
                Besides, you shouldn't use meter element to indicate a progress such as 80% because there is 
                a "progress" element in HTML5.
            </p>
            <p>
                One example is score of rating. 
                I would rate this movie &lt;meter min="0" max="10" value="8"&gt;8 of 10&lt;/meter&gt;.
            </p>
            <p>
                Below is the scores of alex's mid-term exam.
            </p>
            <p>    
                Science : <meter min="0" max="100" value="95">95 of 100</meter> <br />
                Math : <meter min="0" max="100" value="60">60 of 100</meter><br />
                Geography : <meter min="0" max="100" value="20">20 of 100</meter> <br />
                History : <meter min="0" max="100" value="50">50 of 100</meter>
            </p>
            <code>
                Science : &lt;meter min="0" max="100" value="95"&gt;95 of 100&lt;/meter&gt; &lt;br /&gt;<br />
                Math : &lt;meter min="0" max="100" value="60"&gt;60 of 100&lt;/meter&gt;&lt;br /&gt;<br />
                Geography : &lt;meter min="0" max="100" value="20"&gt;20 of 100&lt;/meter&gt; &lt;br /&gt;<br />
                History : &lt;meter min="0" max="100" value="50"&gt;50 of 100&lt;/meter&gt;            
            </code>
            
            <p>
                As of writing, only Opera 11 and Google Chrome 8 render "meter" element into a visualize gauge.
                Therefore, it is good to specify textual value in between "meter" tags, so web browsers don't 
                render "meter" as gauge can simply show its value in plain text.
            </p>
            <p>
                Without your effort to specify a textual value in between "meter" tag, user will just see a blank space
                in web browsers that don't support "meter".
            </p>
            
            <table class="browser">
                <thead>
                    <tr><th>Browsers</th><th>Render meter element as gauge</th></tr>
                </thead>
                <tbody>
                    <tr><td>IE 9 Beta</td><td></td></tr>
                    <tr><td>Firefox 13</td><td></td></tr>
                    <tr><td>Safari 5</td><td></td></tr>
                    <tr><td>Chrome 8</td><td>&#10003;</td></tr>
                    <tr><td>Opera 11</td><td>&#10003;</td></tr>
                </tbody>
            </table>
            <p>
                If you do not have the web browser that supports "meter" element, below is how it is seen in Opera 11.
            </p>
            
            <img class="clipart" src="images/exam-scores.png" alt="meter element rendered in Opera"/>
            
            <p>
                Besides "min", "max" and "value" attribute, there are 3 more attributes of meter element which 
                you can use to further enhance your gauge.
            </p>
            
            <table class="attribute">
                <thead>
                    <tr>
                        <th>Attributes</th>
                        <th>Descriptions</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>value</td>
                        <td>You need to specify a value in number in order to use "meter" element. 
                        This is an mandatory attribute, No excuse! 
                        The number can be integer or floating point number.
                        </td>
                    </tr>                  
                    <tr>
                        <td>min</td>
                        <td>Minimum value of meter element. If unspecified, it will be Zero (0).
                        </td>
                    </tr>
                    <tr>
                        <td>max</td>
                        <td>Maximum value of meter element. If unspecified, it will be One (1).</td>
                    </tr>
                    <tr>
                        <td>low</td>
                        <td>This is optional unless you wish to indicate the low value of the range.</td>
                    </tr> 
                    <tr>
                        <td>high</td>
                        <td>This is optional unless you wish to define high value of the range.</td>
                    </tr>                                       
                    <tr>
                        <td>optimum</td>
                        <td>This is completely optional. This attribute is for specifying an optimum point of a range.</td>
                    </tr>                    
                </tbody>
            </table>            
            
            
        </article>
<?php include("page_footer.php"); ?>   