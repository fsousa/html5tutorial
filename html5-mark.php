<?php $lesson=340; ?>
<?php include("page_header.php"); ?> 
    <article>
        <header>
            <h1><?php echo $lesson_arr[$current_id][1]?></h1>
        </header>
<script type="text/javascript"><!--
google_ad_client = "ca-pub-3162865608794255";
/* Html5 below title */
google_ad_slot = "6072872264";
google_ad_width = 728;
google_ad_height = 15;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script> 

            <p> 
                The mark &lt;mark&gt; element represents a run of text in one document marked or 
                highlighted for reference purposes, due to its relevance in another context.
            </p>
            <p>
                Basically, it is used to bring the reader's 
                attention to <mark>a part of the text</mark> that might not have been considered important
                or overlooked.
            </p>
            
            <p>
                In the sentence above, I have highlighted "a part of the text" because I think that is a very important point 
                that you must not overlook.
            </p>
            
            <p>
                To mark part of the text, simply put your text in between &lt;mark&gt;&lt;/mark&gt;. It is that easy.
            </p>
            
            <code>
                Basically, it is used to bring the reader's 
                attention to <mark>&lt;mark&gt;a part of the text&lt;/mark&gt;</mark> that might not have been considered important
                or overlooked.
            </code>
            
            <p>
                As of writing, not every web browser renders &lt;mark&gt;&lt;/mark&gt; as highlighted text, to be 
                on the safe side, it is also good you style the <mark>mark</mark> element in CSS.
            </p>
            
            <code>
                mark {
                    background-color:#ff9;
                    color:#000; 
                    font-style:normal;
                    font-weight:bold;
                }
            
            </code>
        </article>
<?php include("page_footer.php"); ?>   