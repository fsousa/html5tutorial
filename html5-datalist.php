<?php $lesson=260; ?>
<?php include("page_header.php"); ?> 
    <article>
        <header>
            <h1><?php echo $lesson_arr[$current_id][1]?></h1>
        </header>
<script type="text/javascript"><!--
google_ad_client = "ca-pub-3162865608794255";
/* Html5 below title */
google_ad_slot = "6072872264";
google_ad_width = 728;
google_ad_height = 15;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script> 
            <p>
                Datalist is seem like type-ahead auto suggest textbox as you can see in 
                <a href="http://www.google.com">Google</a> search box. Of course, it is only "seem-like"
                but not even close to it.
            </p>
            <p>    
                Datalist is like a "Select" dropdown list but user can freely input anything in the textbox other
                than what are listed in dropdown. As with &lt;SELECT&gt; dropdown everything is in client side.
            </p>
            
            <p>
                Before we get into the code and demo, let's first check out which browser
                that is currently supporting it.
            </p>

            <table class="browser">
                <thead>
                    <tr><th>Browsers</th><th>Datalist Support</th></tr>
                </thead>
                <tbody>
                    <tr><td>IE 9 Beta</td><td></td></tr>
                    <tr><td>Firefox 4</td><td>&#10003;</td></tr>
                    <tr><td>Safari 5</td><td></td></tr>
                    <tr><td>Chrome 20</td><td>&#10003;</td></tr>
                    <tr><td>Opera 11</td><td>&#10003;</td></tr>
                </tbody>
            </table>

            <p>
                Let's look into how to accomplish HTML5 Datalist. Pretty simple!
            </p>
            
            <code>
            &lt;label for="country_name"&gt;Country : &lt;/label&gt;&lt;input id="country_name" name="country_name" type="text" list="country" /&gt;<br />
            &lt;<mark>datalist</mark> id="country"&gt;<br />
            &nbsp;&nbsp;    &lt;option value="Afghanistan"&gt;<br />
            &nbsp;&nbsp;    &lt;option value="Albania"&gt;<br />
            &nbsp;&nbsp;    &lt;option value="Algeria"&gt;<br />
            &nbsp;&nbsp;    &lt;option value="Andorra"&gt;<br />
            &nbsp;&nbsp;    &lt;option value="Angola"&gt;<br />
            &lt;/<mark>datalist</mark>&gt;            
            
            </code>
                    
            <p>
                Feel free to try the Datalist demo! Just enter your country name.
            </p>        
            
            <label for="country_name">Country : </label><input id="country_name" name="country_name" type="text" list="country" />
            <datalist id="country">
                <option value="Afghanistan">
                <option value="Albania">
                <option value="Algeria">
                <option value="Andorra">
                <option value="Angola">
                <option value="Antigua & Deps">
                <option value="Argentina">
                <option value="Armenia">
                <option value="Australia">
                <option value="Austria">
                <option value="Azerbaijan">
                <option value="Bahamas">
                <option value="Bahrain">
                <option value="Bangladesh">
                <option value="Barbados">
                <option value="Belarus">
                <option value="Belgium">
                <option value="Belize">
                <option value="Benin">
                <option value="Bhutan">
                <option value="Bolivia">
                <option value="Bosnia Herzegovina">
                <option value="Botswana">
                <option value="Brazil">
                <option value="Brunei">
                <option value="Bulgaria">
                <option value="Burkina">
                <option value="Burundi">
                <option value="Cambodia">
                <option value="Cameroon">
                <option value="Canada">
                <option value="Cape Verde">
                <option value="Central African Rep">
                <option value="Chad">
                <option value="Chile">
                <option value="China">
                <option value="Colombia">
                <option value="Comoros">
                <option value="Congo">
                <option value="Congo {Democratic Rep}">
                <option value="Costa Rica">
                <option value="Croatia">
                <option value="Cuba">
                <option value="Cyprus">
                <option value="Czech Republic">
                <option value="Denmark">
                <option value="Djibouti">
                <option value="Dominica">
                <option value="Dominican Republic">
                <option value="Timor Leste">
                <option value="Ecuador">
                <option value="Egypt">
                <option value="El Salvador">
                <option value="Equatorial Guinea">
                <option value="Eritrea">
                <option value="Estonia">
                <option value="Ethiopia">
                <option value="Fiji">
                <option value="Finland">
                <option value="France">
                <option value="Gabon">
                <option value="Gambia">
                <option value="Georgia">
                <option value="Germany">
                <option value="Ghana">
                <option value="Greece">
                <option value="Grenada">
                <option value="Guatemala">
                <option value="Guinea">
                <option value="Guinea-Bissau">
                <option value="Guyana">
                <option value="Haiti">
                <option value="Honduras">
                <option value="Hungary">
                <option value="Iceland">
                <option value="India">
                <option value="Indonesia">
                <option value="Iran">
                <option value="Iraq">
                <option value="Ireland {Republic}">
                <option value="Israel">
                <option value="Italy">
                <option value="Ivory Coast">
                <option value="Jamaica">
                <option value="Japan">
                <option value="Jordan">
                <option value="Kazakhstan">
                <option value="Kenya">
                <option value="Kiribati">
                <option value="Korea North">
                <option value="Korea South">
                <option value="Kuwait">
                <option value="Kyrgyzstan">
                <option value="Laos">
                <option value="Latvia">
                <option value="Lebanon">
                <option value="Lesotho">
                <option value="Liberia">
                <option value="Libya">
                <option value="Liechtenstein">
                <option value="Lithuania">
                <option value="Luxembourg">
                <option value="Macedonia">
                <option value="Madagascar">
                <option value="Malawi">
                <option value="Malaysia">
                <option value="Maldives">
                <option value="Mali">
                <option value="Malta">
                <option value="Marshall Islands">
                <option value="Mauritania">
                <option value="Mauritius">
                <option value="Mexico">
                <option value="Micronesia">
                <option value="Moldova">
                <option value="Monaco">
                <option value="Mongolia">
                <option value="Montenegro">
                <option value="Morocco">
                <option value="Mozambique">
                <option value="Myanmar, {Burma}">
                <option value="Namibia">
                <option value="Nauru">
                <option value="Nepal">
                <option value="Netherlands">
                <option value="New Zealand">
                <option value="Nicaragua">
                <option value="Niger">
                <option value="Nigeria">
                <option value="Norway">
                <option value="Oman">
                <option value="Pakistan">
                <option value="Palau">
                <option value="Panama">
                <option value="Papua New Guinea">
                <option value="Paraguay">
                <option value="Peru">
                <option value="Philippines">
                <option value="Poland">
                <option value="Portugal">
                <option value="Qatar">
                <option value="Romania">
                <option value="Russian Federation">
                <option value="Rwanda">
                <option value="St Kitts & Nevis">
                <option value="St Lucia">
                <option value="Saint Vincent & the Grenadines">
                <option value="Samoa">
                <option value="San Marino">
                <option value="Sao Tome & Principe">
                <option value="Saudi Arabia">
                <option value="Senegal">
                <option value="Serbia">
                <option value="Seychelles">
                <option value="Sierra Leone">
                <option value="Singapore">
                <option value="Slovakia">
                <option value="Slovenia">
                <option value="Solomon Islands">
                <option value="Somalia">
                <option value="South Africa">
                <option value="Spain">
                <option value="Sri Lanka">
                <option value="Sudan">
                <option value="Suriname">
                <option value="Swaziland">
                <option value="Sweden">
                <option value="Switzerland">
                <option value="Syria">
                <option value="Taiwan">
                <option value="Tajikistan">
                <option value="Tanzania">
                <option value="Thailand">
                <option value="Togo">
                <option value="Tonga">
                <option value="Trinidad & Tobago">
                <option value="Tunisia">
                <option value="Turkey">
                <option value="Turkmenistan">
                <option value="Tuvalu">
                <option value="Uganda">
                <option value="Ukraine">
                <option value="United Arab Emirates">
                <option value="United Kingdom">
                <option value="United States of America">
                <option value="Uruguay">
                <option value="Uzbekistan">
                <option value="Vanuatu">
                <option value="Vatican City">
                <option value="Venezuela">
                <option value="Vietnam">
                <option value="Yemen">
                <option value="Zambia">
                <option value="Zimbabwe">
            </datalist>
            
            <p>
                If you do not have the browser that support it, here is how it looks like in Firefox 4.
            </p>
            
            <img class="clipart" src="images/datalist.png" alt="Datalist" title="Datalist" />
        </article>
<?php include("page_footer.php"); ?>   
