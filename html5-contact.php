<?php $lesson=280; ?>
<?php include("page_header.php"); ?> 
    <article>
        <header>
            <h1><?php echo $lesson_arr[$current_id][1]?></h1>
        </header>
<script type="text/javascript"><!--
google_ad_client = "ca-pub-3162865608794255";
/* Html5 below title */
google_ad_slot = "6072872264";
google_ad_width = 728;
google_ad_height = 15;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>             
            <p>
                Email, Phone number and Website url are the three most commonly used 
                contact detail which has its unique input patterns. 
                
            </p>
            <p>
                Email has an @ and ending with ".com" or ".net" or ".something else", 
                website has at least one dot in the middle and most of them ending with ".com",
                and phone number is just number without alphabet.
            </p>
            
            <p>
                Don't be confuse, these types are not for data validation, at least not yet. You will have to use
                the same regular expression to do so.
            </p>
            
            <p>
                Below is how it looks like, you may try to input something into each field 
                and you will find nothing special compared to plain textbox (&lt;input type="text"/&gt;).
            </p>
            
            <table class="browser">
                <thead><tr><th colspan="2">html-5.my demo</th></tr></thead>
                <tbody>
                    <tr>
                        <td><label for="email">Email Address</label></td><td><input id="email" type="email" /></td>
                    </tr>
                    <tr>
                        <td><label for ="website">Web Site</label></td><td> <input id="website" type="url" /></td>
                    </tr>
                    <tr>
                        <td><label for="phone" >Phone Number</label></td><td><input id="phone" type="tel" /></td>
                    </tr>                                        
                </tbody>                
            </table>
            <p>
                Here is how the code looks like.
            </p>
            <code>
                &lt;input id="email" <mark>type="email"</mark>/&gt;<br />
                &lt;input id="website" <mark>type="url"</mark>/&gt;<br />
                &lt;input id="phone" <mark>type="tel"</mark> /&gt;                
            </code>
            <p>
                 However, there are differences in how touch screen based web browsers treat those field. Below is
                 the screen capture of iPhone 4 (iOS 4.2) and HTC Desire (Android 2.2) when each of the field gets
                 focus.
            </p>            
               <img class="clipart" src="images/iphone_contact_details.png" alt="html5 input type in iphone" title="html5 input type in iphone"/>
            <p>
                As you can see, the web browser will give you a number pad when you focus on phone number field (type="tel"),
                iphone even take it further to make input of email and web url easier, every email has an "@" sign and most of the
                web url ending with ".com".
           </p>
           <p>     
                On the other hand, My Android keyboard input is not as friendly as iPhone is as of writing.
            </p>
               <img class="clipart" src="images/android_contact_detail.png" alt="html5 input type in android" title="html5 input type in android" />                   
            
            <p>
                In conclusion, there is no reason for you not to use these "Types" from now, for web browsers that does not 
                regconize these new types will just treat it as plain text (&lt;input type="text"/&gt;). 
                This will make your page more input friendly to mobile users and more readable by machine, this is 
                the spirit of Semantic web.
            </p>       

        </article>
<?php include("page_footer.php"); ?>   