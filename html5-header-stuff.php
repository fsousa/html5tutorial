<?php $lesson=130; ?>
<?php include("page_header.php"); ?> 
    <article>
        <header>
            <h1><?php echo $lesson_arr[$current_id][1]?></h1>
        </header>
<script type="text/javascript"><!--
google_ad_client = "ca-pub-3162865608794255";
/* Html5 below title */
google_ad_slot = "6072872264";
google_ad_width = 728;
google_ad_height = 15;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>             
            <p>
                Very often in the header of our web pages, we specify the Content Type, Javascript and CSS file path,
                again, life is easier with <abbr>HTML5</abbr>.
            </p>
            <p>Instead of </p>
            <code>
            &lt;meta http-equiv="Content-Type" content="text/html; charset=UTF-8"&gt;
            </code>
            <p>In <abbr>HTML5</abbr>, it can be just :-</p>
            <code>
                &lt;meta charset="UTF-8"&gt
            </code>
            
            <p>It used to be :-</p>
            
            <code>&lt;script type="text/javascript" src="jquery.js"&gt;&lt;/script&gt;</code>
            
            <p>In <abbr>HTML5</abbr>, redundant information "type=text/javascript" is no longer necessary, practically, this is the only client side script alive.</p>
            
            <code>&lt;script src="jquery.js"&gt;&lt;/script&gt;</code>
            
            <p>Similarly, CSS receives the first class citizen treatment as javascript too. You can forget about "type=text/css" from now on.</p>
            
            <code>&lt;link rel="stylesheet" href="html-my.css"&gt;</code>
            
            <p>If this is the way your &lt;html&gt; root looks like </p>
            
            <code>&lt;html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en"&gt;</code>
            
            <p>You can now simplify with simply as below : -</p>
            
            <code>&lt;html lang="en""&gt;</code>
        </article>
<?php include("page_footer.php"); ?>   
