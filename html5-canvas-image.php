<?php $lesson=200; ?>
<?php include("page_header.php"); ?> 
    <article>
        <header>
            <h1><?php echo $lesson_arr[$current_id][1]?></h1>
        </header>
<script type="text/javascript"><!--
google_ad_client = "ca-pub-3162865608794255";
/* Html5 below title */
google_ad_slot = "6072872264";
google_ad_width = 728;
google_ad_height = 15;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script> 
            <h2>Place images in Canvas</h2>
           
            <p>
                Drawing lines or simple shape is boring, let's draw some pictures!
            </p>
            
            <div class="canvas_demo"><canvas id="c5" width="600" height = "300" style="border:solid 1px #000000;"></canvas>
                <div>
                    <button class="canvas_button" onclick="draw_dragon();return true;">Draw Dragon</button>
                    <button class="canvas_button" onclick="draw_smaller_dragon();return true;">Draw smaller dragon</button>
                    <button class="canvas_button" onclick="draw_dragon_head();return true;">Draw Dragon Head</button>
                    <button class="canvas_button" onclick="Clear_image();return true;">Erase Everything</button>
                </div>            
            </div>
            
            <script>
                var c5 = document.getElementById("c5");
                var c5_context = c5.getContext("2d");
                
                var dragon = new Image();
                dragon.src = 'images/chinese_dragon.png';

                function draw_dragon() {
                    c5_context.drawImage(dragon, 100, 5);
                }

                function draw_smaller_dragon() {
                    c5_context.drawImage(dragon, 10, 5, 58, 100);
                }

                function draw_dragon_head() {
                    c5_context.drawImage(dragon, 0, 19, 69, 97, 300, 100,103,145);
                }

                function Clear_image() {
                    c5_context.clearRect(1, 1, 600, 300);
                }                
            
            </script>            
            <p>
                In above demo, I use only one context method, "drawImage()". However, "drawImage" method can have
                3,5 or 9 arguments. I have three functions which are "draw_dragon()", "draw_smaller_dragon()" and "draw_dragon_head()",
                each of them implements 3,5 and 9 arguments respectively.
                
            </p>
           
            <table class="attribute">
                <thead>
                    <tr>
                        <th>Overloading of drawImage()</th><th>Description</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>3 arguments <br />drawImage (img,x,y)</td>
                        <td>img is the image element.<br />
                            x and y being the coordinate to place the image object.
                        </td>
                    </tr>
                    <tr>
                        <td>5 arguments <br />drawImage (img,x,y,width,height)</td>
                        <td>img is the image element.<br />
                            x and y being the coordinate to place the image object.
                            width and height will be image size that you want, resize as you want. 
                        </td>
                    </tr>
                    <tr>
                        <td>9 arguments <br />drawImage (img, crop_x, crop_y, crop_width, crop_height, x, y, width, height)</td>
                        <td>img is the image element.<br />
                            crop_x and crop_y is where you start cropping your image.<br />
                            crop_width and crop_height is the size you want to crop your image.<br />
                            x and y still being  the coordinate to place the image object.<br />
                            width and height will be image size that you want, resize as you like it.
                        
                        </td>
                    </tr>                                        
                </tbody>
            </table>
            
            <p>Here is the code of of the dragon demo.</p>
            <code>
            &lt;div"&gt;&lt;canvas id="c5" width="600" height = "300" style="border:solid 1px #000000;"&gt;&lt;/canvas&gt;<br />
                &lt;div&gt;<br />
                    &lt;button onclick="draw_dragon();return true;"&gt;Draw Dragon&lt;/button&gt;<br />
                    &lt;button onclick="draw_smaller_dragon();return true;"&gt;Draw smaller dragon&lt;/button&gt;<br />
                    &lt;button onclick="draw_dragon_head();return true;"&gt;Draw Dragon Head&lt;/button&gt;<br />
                    &lt;button onclick="Clear_image();return true;"&gt;Erase Everything&lt;/button&gt;<br />
                &lt;/div&gt;            <br />
            &lt;/div&gt;<br />
            <br />
            &lt;script&gt;<br />
                var c5 = document.getElementById("c5");<br />
                var c5_context = c5.getContext("2d");<br />
                <br />
                var dragon = new Image();<br />
                dragon.src = 'images/chinese_dragon.png';<br />
<br />
                function draw_dragon() {<br />
                    c5_context.<mark>drawImage(dragon, 100, 5)</mark>;<br />
                }<br />
<br />
                function draw_smaller_dragon() {<br />
                    c5_context.<mark>drawImage(dragon, 10, 5, 58, 100)</mark>;<br />
                }<br />
<br />
                function draw_dragon_head() {<br />
                    c5_context.<mark>drawImage(dragon, 0, 19, 69, 97, 300, 100, 103, 145)</mark>;<br />
                }<br />
<br />
                function Clear_image() {<br />
                    c5_context.clearRect(1, 1, 600, 300);<br />
                }                <br />
            &lt;/script&gt;  
            </code>
        </article>
 
<?php include("page_footer.php"); ?>   
