<?php $lesson=120; ?>
<?php include("page_header.php"); ?> 
    <article>
        <header>
            <h1><?php echo $lesson_arr[$current_id][1]?></h1>
        </header>
<script type="text/javascript"><!--
google_ad_client = "ca-pub-3162865608794255";
/* Html5 below title */
google_ad_slot = "6072872264";
google_ad_width = 728;
google_ad_height = 15;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>         
        
            <p>Document Type Declaration in <abbr>HTML5</abbr> is cool and simple. It is as simple as below:-</p>
            <code> &lt;!DOCTYPE html&gt;</code>
            
            <p>
                Unlike of different variant <abbr>HTML4</abbr> or <abbr>XHTML1</abbr> doctype that we are familiar with:-
            </p>
    
            <code>
                
                &lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.01//EN&quot; 
                &quot;http://www.w3.org/TR/html4/strict.dtd&quot;&gt; 
                <br /><br />
                
                &lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 
                4.01 Transitional//EN&quot; &quot;http://www.w3.org/TR/html4/loose.dtd&quot;&gt; 
                <br /><br />
                
                
                &lt;!DOCTYPE HTML 
                PUBLIC &quot;-//W3C//DTD HTML 4.01 Frameset//EN&quot; 
                &quot;http://www.w3.org/TR/html4/frameset.dtd&quot;&gt; 
                <br /><br />
                
                
                &lt;!DOCTYPE html PUBLIC &quot;-//W3C//DTD 
                XHTML 1.0 Strict//EN&quot; &quot;http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd&quot;&gt; 
                <br /><br />
                
                
                &lt;!DOCTYPE html PUBLIC &quot;-//W3C//DTD XHTML 1.0 Transitional//EN&quot; 
                &quot;http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd&quot;&gt;
                
             </code>
             
             <p>
                Some of them can't even be fitted in one line. 
                This is the first proof that web designer life will be easier with <abbr>HTML5</abbr>.
                A very simple <abbr>HTML5</abbr> web page can be as simple as below.
             </p>
             
             <code>
                &lt;!DOCTYPE html&gt;<br />
                &lt;html&gt;<br />
                &lt;head&gt;<br />
                &nbsp;&nbsp;    &lt;title&gt;HTML5 is rocks!&lt;/title&gt;<br />
                &lt;/head&gt;<br />
                &lt;body&gt;<br />
                &nbsp;&nbsp;    &lt;h1&gt;HTML5 Rocks!&lt;/h1&gt;<br />
                &lt;/body&gt;<br />
                &lt;/html&gt;<br />
             
             </code>
             
             
               
        </article>
<?php include("page_footer.php"); ?>   