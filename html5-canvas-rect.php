<?php $lesson=170; ?>
<?php include("page_header.php"); ?> 
    <article>
        <header>
            <h1><?php echo $lesson_arr[$current_id][1]?></h1>
        </header>
<script type="text/javascript"><!--
google_ad_client = "ca-pub-3162865608794255";
/* Html5 below title */
google_ad_slot = "6072872264";
google_ad_width = 728;
google_ad_height = 15;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>         
            <h2>How to draw Rectangle?</h2>
            
            <p>
                Before dreaming about creating an interactive HTML5 games, let's start with the basic.
                
            </p>
            <div class="canvas_demo">
                <canvas id="c1" width="200" height="200" style="border:solid 1px #000000;"></canvas> <br />
                <button class="canvas_button" onclick="draw_square();return true;">Red Square</button>
            </div>

            <script>
                function draw_square() {
                    var c1 = document.getElementById("c1");
                    var c1_context = c1.getContext("2d");
                    c1_context.fillStyle = "#f00";
                    c1_context.fillRect(50, 50, 100, 100);
                }
            </script>
                            
            <p>First, click the "Red Square" button, if you don't see a red square in the canvas, 
            you problably want to get the right <a href="html5-canvas.php#browser">web browser</a> to continue.</p>

            <code>
                &lt;canvas id="c1" width="200" height="200" style="border:solid 1px #000000;"&gt;&lt;/canvas&gt;<br />
                &lt;button  onclick="draw_square();return true;"&gt;Red Square&lt;/button&gt;
                &lt;script&gt;<br />
                    function draw_square() {<br />
                    &nbsp;&nbsp;    var c1 = document.getElementById("c1");<br />
                    &nbsp;&nbsp;    var c1_context = c1.getContext("2d");<br />
                    &nbsp;&nbsp;    c1_context.fillStyle = "#f00";<br />
                    &nbsp;&nbsp;    c1_context.fillRect(50, 50, 100, 100);<br />
                    }<br />
                &lt;/script&gt;<br />                            
            </code>
            
            <p>
                To draw anything in canvas, you do not put anything between the opening tag and closing tag of &lt;canvas&gt;, 
                browsers that supports Canvas will just ignore it. You can only use Javascript to do your drawing.
            </p>                
            
            <p>
                Rule number one, your canvas element has got to have an ID, so we can use Javascript to locate it.
                Secondly, every canvas has what we call "Context". In fact, context of canvas is what we are going to paint on, not canvas itself.
            </p>
            
            <code>
                    &nbsp;&nbsp;    var c1 = document.getElementById("c1");<br />
                    &nbsp;&nbsp;    var c1_context = c1.getContext("2d");<br />            
            </code>
            
            <aside>
                As of writing, not only 2D context is available but also a 3D one. 
                However, the 3D context which is called WebGL is not brought to you by <abbr>W3C</abbr> or <abbr>WHATWG</abbr>  but a 
                non-for-profit organization named <a href="http://www.khronos.org/">Khronos Group</a>.
                Added to that, Google has released an amazing product <a href="http://bodybrowser.googlelabs.com/">Body Browser</a> in Dec 2010 to further ensure the future of this standard.
                
            </aside>
            
            <p>
                Let's choose use the red paint (fillStyle="#f00";) and paint a red square with 100px width and height. (fillRect(50, 50, 100, 100)).
            </p>
            <code>
                    &nbsp;&nbsp;    c1_context.fillStyle = "#f00";<br />
                    &nbsp;&nbsp;    c1_context.fillRect(50, 50, 100, 100);<br />
            </code>
            
            <p>
                The table below shows the methods of drawing rectangle.
            </p>
            
            <table class="attribute">
                <thead>
                    <tr><th>Methods of "Context"</th><th>Descriptions</th></tr>
                </thead>
                <tbody>
                    <tr>
                        <td>fillStyle</td>
                        <td>CSS Color, pattern or gradient within the shape. Default fillStyle is solid black color.</td>
                    </tr>
                    <tr>
                        <td>strokeStyle</td>
                        <td>Color or CSS style of the lines of the shape</td>
                    </tr> 
                    <tr>
                        <td>fillRect(x, y, width, height)</td>
                        <td>Draw a rectangle start from coordinate x and y and size of width x height.</td>
                    </tr>
                    <tr>
                        <td>strokeRect(x, y, width, height)</td>
                        <td>Draw a rectangle with just its edges.</td>
                    </tr>
                    <tr>
                        <td>clearRect(x, y, width, height)</td>
                        <td>Clear the area specified in x, y coordinate and area of width x height</td>
                    </tr>                                                               
                </tbody>
            </table>
         
            <div class="canvas_demo"><canvas id="c2" width="200" height = "200" style="border:solid 1px #000000;"></canvas>
                <div>
                    <button class="canvas_button" onclick="blue_square_2();return true;">Blue Square</button>
                    <button class="canvas_button" onclick="red_stroke_2();return true;">Red Edges</button>
                    <button class="canvas_button" onclick="clear_rect_2();return true;">Erase Everything</button>
                </div>            
            </div>
            
            <script>
                var c2 = document.getElementById("c2");
                var c2_context = c2.getContext("2d");

                function blue_square_2() {                      //Blue color square
                    c2_context.fillStyle = "#00f";
                    c2_context.fillRect(50, 50, 100, 100);
                }

                function red_stroke_2() {                       //Red color edges
                    c2_context.strokeStyle = "#f00";
                    c2_context.strokeRect(45, 45, 110, 110);
                }

                function clear_rect_2() {                       //Clear all
                    c2_context.clearRect(40, 40, 120, 120);
                }
            </script>
            
            <code>
            &lt;div &gt;&lt;canvas id="Canvas2" width="200" height = "200" style="border:solid 1px #000000;"&gt;&lt;/canvas&gt;<br />
            &nbsp;&nbsp;    &lt;div&gt;<br />
            &nbsp;&nbsp&nbsp;&nbsp        &lt;button  onclick="blue_square_2();return true;"&gt;Blue Square&lt;/button&gt;<br />
            &nbsp;&nbsp&nbsp;&nbsp         &lt;button onclick="red_stroke_2();return true;"&gt;Red Square&lt;/button&gt;<br />
            &nbsp;&nbsp&nbsp;&nbsp         &lt;button  onclick="clear_rect_2();return true;"&gt;Erase Everything&lt;/button&gt;<br />
            &nbsp;&nbsp;    &lt;/div&gt;       <br />     
            &lt;/div&gt;<br />
            
            &lt;script&gt;<br />
            &nbsp;&nbsp;    var c2 = document.getElementById("c2");<br />
            &nbsp;&nbsp;    var c2_context = c2.getContext("2d");<br />

            &nbsp;&nbsp;    function blue_square_2() {                  //Blue color square<br /> 
            &nbsp;&nbsp&nbsp;&nbsp         c2_context.fillStyle = "#00f";<br />
            &nbsp;&nbsp&nbsp;&nbsp         c2_context.fillRect(50, 50, 100, 100);<br />
            &nbsp;&nbsp;    }<br />

            &nbsp;&nbsp;     function red_stroke_2() {                  //Red color edges<br />
            &nbsp;&nbsp&nbsp;&nbsp         c2_context.strokeStyle = "#f00";<br />
            &nbsp;&nbsp&nbsp;&nbsp         c2_context.strokeRect(45, 45, 110, 110);<br />
            &nbsp;&nbsp;    }<br />

            &nbsp;&nbsp;    function clear_rect_2() {                   //Clear all<br />
            &nbsp;&nbsp&nbsp;&nbsp         c2_context.clearRect(40, 40, 120, 120);<br />
            &nbsp;&nbsp;    }<br />
            &lt;/script&gt;           
            
            </code>
        </article>
<?php include("page_footer.php"); ?>   