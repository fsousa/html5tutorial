<?php $lesson=320; ?>
<?php include("page_header.php"); ?> 
    <article>
        <header>
            <h1><?php echo $lesson_arr[$current_id][1]?></h1>
        </header>
<script type="text/javascript"><!--
google_ad_client = "ca-pub-3162865608794255";
/* Html5 below title */
google_ad_slot = "6072872264";
google_ad_width = 728;
google_ad_height = 15;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>         
            <p> 
                In HTML5, we can have color input with simply &lt;input type="color"&gt;. 
                The textbox should only carry value of so called "simple color" string in lowercase
                such as #ff0000 for Red Color, #000000 for Black Color and #ffffff for White color.
            </p>
            <p>
                As of writing, the only web browser that support color input is Opera 11.
            </p>
            <table class="browser">
                <thead>
                    <tr><th>Browsers</th><th>Color input Support</th></tr>
                </thead>
                <tbody>
                    <tr><td>IE 9 Beta</td><td></td></tr>
                    <tr><td>Firefox 13</td><td></td></tr>
                    <tr><td>Safari 5</td><td></td></tr>
                    <tr><td>Chrome 20</td><td>&#10003;</td></tr>
                    <tr><td>Opera 11</td><td>&#10003;</td></tr>
                </tbody>
            </table>
            
            <p>
                With Color input type, you no longer need a complex Javascript color picker, 
                a simple line of code below will do the work.
            </p>
            
            <code>
                &lt;label for="background-color"&gt;Choose a calor for background :&lt;/label&gt;
                <br />
                &lt;input id="background-color" <mark>type="color"</mark> /&gt;            
            </code>
            <img class="clipart" src="images/opera-color1.png" alt="Color input in Opera 11" title="Color input in Opera 11" />
            
            <img class="clipart" src="images/opera-color2.png" alt="Color input in Opera 11" title="Color input in Opera 11"  />
            <p>
                If you are lucky enough to have web browser that support color input, please feel free to try the demo below.               
            </p>
            <div>
                <label for="background-color">Choose a calor for background : </label>
                <input id="background-color" type="color" value="#ff0000" onchange="javascript:document.getElementById('chosen-color').value = document.getElementById('background-color').value;"/>
            </div>
            <div>
                <label for="chosen-color">You have chosen : </label>
                <input id="chosen-color" type="text" readonly value="#ff0000"/>
            </div>
        </article>
<?php include("page_footer.php"); ?>   