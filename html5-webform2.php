<?php $lesson=220; ?>
<?php include("page_header.php"); ?> 
    <article>
        <header>
            <h1><?php echo $lesson_arr[$current_id][1]?></h1>
        </header>
<script type="text/javascript"><!--
google_ad_client = "ca-pub-3162865608794255";
/* Html5 below title */
google_ad_slot = "6072872264";
google_ad_width = 728;
google_ad_height = 15;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>             
            <p>
                The evolution of HTML5 is to make a better World Wide Web. Writing less code is good (especially javascript), 
                at the end of the day, those repetitive works of web developers should be 
                taken over by web browsers. After all, this is the main reason man invented machine.
            </p>
            <p>
                What you can see below is a list of new input attributes and input types, 
                we shall go through each and every one.
            </p>
            <p>
                New Input attributes &lt;input type="text" "<mark>new input attribute</mark>"/&gt;
            </p>
            <ul>
                <li>Placeholder</li>
                <li>Autofocus</li>
                <li>Required</li>
                <li>DataList</li>
            </ul>
            <p>
                For years, We are only given a handful of "Type"s to be used in Input element, such as "text", "radio", "checkbox", "password", "file" and "submit",
                In HTML5, we will be having more fun with the new kids on the block.
            </p>
            <p>    
                New input types &lt;input type="<mark>new input type</mark>"/&gt;
            </p>
            <ul>    
                <li>Search</li>
                <li>Email, URL and Phone</li>
                <li>Range as Slider</li>
                <li>Number as Spinner</li>
                <li>Date and Times</li>
                <li>Color picker</li>
            </ul>
           
           
        </article>
<?php include("page_footer.php"); ?>   
