<?php $lesson=190; ?>
<?php include("page_header.php"); ?> 
    <article>
        <header>
            <h1><?php echo $lesson_arr[$current_id][1]?></h1>
        </header>
<script type="text/javascript"><!--
google_ad_client = "ca-pub-3162865608794255";
/* Html5 below title */
google_ad_slot = "6072872264";
google_ad_width = 728;
google_ad_height = 15;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>             
            <h2>How to draw paths?</h2>
            <p>Let's first draw one line</p>
            <div class="canvas_demo"><canvas id="c3" width="600" height = "200" style="border:solid 1px #000000;"></canvas>
                <div>
                    <button class="canvas_button" onclick="Vertical_line();return true;">Click me to draw a brown vertical line</button>
                </div>            
            </div>
                        
            <script>
                var c3 = document.getElementById("c3");
                var c3_context = c3.getContext("2d");

                function Vertical_line() {
                    c3_context.moveTo(300, 10);
                    c3_context.lineTo(300, 190);
                    c3_context.strokeStyle = "brown";
                    c3_context.stroke();
                }
            </script>
            <p>
                Here you go, the script that drew the brown line above.
            </p>
            <code>
            &lt;div&gt;&lt;canvas id="Canvas2" width="600" height = "200" style="border:solid 1px #000000;"&gt;&lt;/canvas&gt;<br />
            &nbsp;&nbsp;    &lt;div&gt;<br />
            &nbsp;&nbsp&nbsp;&nbsp        &lt;button onclick="Vertical_line();return true;"&gt;Click me to draw a brown vertical line&lt;/button&gt;<br />
            &nbsp;&nbsp;    &lt;/div&gt;  <br />          
            &lt;/div&gt;<br />
                        
            &lt;script&gt;<br />
            &nbsp;&nbsp;    var c3 = document.getElementById("c3");<br />
            &nbsp;&nbsp;    var c3_context = c3.getContext("2d");<br /><br />

            &nbsp;&nbsp;    function Vertical_line() {<br />
            &nbsp;&nbsp&nbsp;&nbsp        c3_context.<mark>moveTo(300, 10);</mark><br />
            &nbsp;&nbsp&nbsp;&nbsp        c3_context.<mark>lineTo(300, 190);</mark><br />
            &nbsp;&nbsp&nbsp;&nbsp        c3_context.<mark>strokeStyle = "brown";</mark><br />
            &nbsp;&nbsp&nbsp;&nbsp        c3_context.<mark>stroke();</mark><br />
            &nbsp;&nbsp;    }<br />
            &lt;/script&gt;            
            </code>
            <p>
                The methods that really matters are "moveTo", "lineTo" ,"stroke" and "strokeStyle".
            </p>
            
            <table class="attribute">
                <thead>
                    <tr><th>Context method</th><th>Descriptions</th></tr>
                </thead>
                <tbody>
                    <tr>
                        <td>moveTo(x,y)</td>
                        <td>move to starting point with coordinate x and y.</td>
                    </tr>
                    <tr>
                        <td>lineTo(x,y)</td>
                        <td>Draw a line to this point from starting point. Again x and y being the coordinate.</td>
                    </tr> 
                    <tr>
                        <td>strokeStyle</td>
                        <td>CSS color of the line</td>
                    </tr>
                    <tr>
                        <td>stroke</td>
                        <td>A method to actually make javascript draw a line</td>
                    </tr>
                    <tr>
                        <td>beginPath</td>
                        <td>Before you start drawing a new line with different color, you will have to call "beginPath".
                        
                        </td>
                    </tr>
                                                             
                </tbody>
            </table>            
                          
            <p>
                Check out the demo below, "beginPath" method is playing an important role. Without it, the last "stroke"
                will redraw the previous line with the last-called "strokeStyle" color.
            </p>
            <div class="canvas_demo"><canvas id="c4" width="600" height = "200" style="border:solid 1px #000000;"></canvas>
                <div>
                    <button class="canvas_button" onclick="Vertical_2px_Red();return true;">Vertical 2px Red line</button>
                    <button class="canvas_button" onclick="Vertical_1px_Blue();return true;">Vertical 1px Blue line</button>
                    <button class="canvas_button" onclick="Horizontal_2px_Green();return true;">Horizontal 2px Green line</button>
                    <button class="canvas_button" onclick="Clear_line();return true;">Erase Everything</button>
                </div>            
            </div>
            
            <script>
                var c4 = document.getElementById("c4");
                var c4_context = c4.getContext("2d");

                function Vertical_2px_Red() {
                    c4_context.beginPath();                     
                    c4_context.moveTo(300, 10);
                    c4_context.lineTo(300, 190);
                    c4_context.strokeStyle = "Red";
                    c4_context.stroke();
                }

                function Vertical_1px_Blue() {
                    c4_context.beginPath();
                    c4_context.moveTo(350.5, 10);
                    c4_context.lineTo(350.5, 190);
                    c4_context.strokeStyle = "Blue";
                    c4_context.stroke();
                }

                function Horizontal_2px_Green() {
                    c4_context.beginPath();                      
                    c4_context.moveTo(100, 100);
                    c4_context.lineTo(500, 100);
                    c4_context.strokeStyle = "Green";
                    c4_context.stroke();
                }

                function Clear_line() {
                    c4_context.clearRect(1, 1, 600, 190);
                }
            </script>
            <code>
            &lt;div&gt;<br />
            &lt;canvas id="c4" width="600" height = "200" style="border:solid 1px #000000;"&gt;&lt;/canvas&gt;<br />
                &lt;div&gt;<br />
                &nbsp;    &lt;button onclick="Vertical_2px_Red();return true;"&gt;Vertical 2px Red line&lt;/button&gt;<br />
                &nbsp;    &lt;button onclick="Vertical_1px_Blue();return true;"&gt;Vertical 1px Blue line&lt;/button&gt;<br />
                &nbsp;    &lt;button onclick="Horizontal_2px_Green();return true;"&gt;Horizontal 2px Green line&lt;/button&gt;<br />
                &nbsp;    &lt;button onclick="Clear_line();return true;"&gt;Erase Everything&lt;/button&gt;<br />
                &lt;/div&gt;            <br />
            &lt;/div&gt;<br />
            <br />
            &lt;script&gt;<br />
                var c4 = document.getElementById("c4");<br />
                var c4_context = c4.getContext("2d");<br />
<br />
                function Vertical_2px_Red() {<br />
                    c4_context.<mark>beginPath();</mark><br />
                    c4_context.moveTo(300, 10);<br />
                    c4_context.lineTo(300, 190);<br />
                    c4_context.strokeStyle = "Red";<br />
                    c4_context.stroke();<br />
                }<br />
<br />
                function Vertical_1px_Blue() {<br />
                    c4_context.<mark>beginPath();</mark><br />
                    c4_context.moveTo(350.5, 10);<br />
                    c4_context.lineTo(350.5, 190);<br />
                    c4_context.strokeStyle = "Blue";<br />
                    c4_context.stroke();<br />
                }<br />
<br />
                function Horizontal_2px_Green() {<br />
                    c4_context.<mark>beginPath();</mark><br />
                    c4_context.moveTo(100, 100);<br />
                    c4_context.lineTo(500, 100);<br />
                    c4_context.strokeStyle = "Green";<br />
                    c4_context.stroke();<br />
                }<br />
<br />
                function Clear_line() {<br />
                    c4_context.clearRect(1, 1, 600, 190);<br />
                }<br />
            &lt;/script&gt;            
            </code>
            <p>
                You may notice in the demo above, there are two buttons to help you draw a vertical line, however, one draws a one pixel line 
                and the other draws a two pixels line. What makes the difference as follows:-
                
            </p>
            <code>
                c4_context.moveTo(<mark>300</mark>, 10);c4_context.lineTo(<mark>300</mark>, 190);
            </code>
            <p>and</p>
            <code>
                c4_context.moveTo(<mark>300.5</mark>, 10);c4_context.lineTo(<mark>300.5</mark>, 190);
            </code> 
            
            <p>
                In order to draw a line with 1 pixel wide, you simply have to shift the coordinate to 0.5 pixel to the left or right (up or down).
                This is because integer value of the coordinate is to be considered at the edge of each pixel. Drawing a line at the 
                edge will simply make the line spread across two adjacent pixels. By shifting the coordinate 0.5 pixel, the line will
                still spread between two pixels but occupy half of each pixel.
                
            </p>
        </article>
    
<?php include("page_footer.php"); ?>   
