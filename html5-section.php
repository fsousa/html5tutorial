<?php $lesson=380; ?>
<?php include("page_header.php"); ?> 
    <article>
        <header>
            <h1><?php echo $lesson_arr[$current_id][1]?></h1>
        </header>
<script type="text/javascript"><!--
google_ad_client = "ca-pub-3162865608794255";
/* Html5 below title */
google_ad_slot = "6072872264";
google_ad_width = 728;
google_ad_height = 15;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script> 
            <p> 
                The specifications of <abbr>W3C</abbr> reads - "The section element represents a generic section of a document or 
                application. 
                A section, in this context, is a <em>thematic grouping of content</em>, typically with a <em>heading</em>".
            </p>
            
            <p>
                In the picture below, what has been pointed are best to use &lt;section&gt;
            </p>
            <img class="clipart" src="images/section.jpg" alt="An example of section" />
            
            <p>
                Beware, the "section" element is not here to replace every single "div" element.
            </p>
            
            <p>
                You should not use &lt;section&gt; element for :
            </p>
            <ol>
                <li>Syndicated content. You do not use &lt;section&gt; for content in the RSS feed. If you are creating a blog website, 
                do not use section element for your blog entry, instead, you should be using article element. However, an article element
                can contain section element(s).</li>
                <li>Purely for layout purpose. Use &lt;div&gt; element instead as generic container for styling purpose.</li>
            </ol>
            
            <code>
            &lt;section&gt;<br />
            &nbsp;    &lt;h1&gt;Application&lt;/h1&gt;<br />
            &nbsp;    &lt;ul&gt;<br />
            &nbsp;&nbsp;        &lt;li&gt;Education&lt;/li&gt;<br />
            &nbsp;&nbsp;        &lt;li&gt;Entertainment&lt;/li&gt;<br />
            &nbsp;&nbsp;        &lt;li&gt;Family&lt;/li&gt;<br />
            &nbsp;&nbsp;        &lt;li&gt;Games&lt;/li&gt;<br />
            &nbsp;&nbsp;        &lt;li&gt;News and Weather&lt;/li&gt;<br />
            &nbsp;    &lt;/ul&gt;<br />
            &lt;/section&gt;            
            </code>
            
            <p>
                You may notice I am using &lt;h1&gt; as heading within section element.
                section element is actually one of <em>sectioning content</em> in <abbr>HTML5</abbr>.
            </p>
            <p>
                A sectioning content ...
            </p>
            <ul>
                <li>is always a subsection of its nearest ancestor such as &lt;body&gt;, &lt;article&gt; or another &lt;section&gt; element.
                </li>
                <li>has their own outline which can be independent from its parent document. You
                are encouraged to use h1 to h6 within section element without bothering what happen outside of the section.
                Simply, that gives you less headache in future.</li>
                <li>
                can have their own &lt;header&gt;, &lt;hgroup&gt; and &lt;footer&gt;
                </li>
            </ul>
            
            <p>
                Therefore, it is perfectly okay if you code it as following. In fact, you are very much encouraged to code so.
            </p>
            
            <code>
                &lt;body&gt;<br />
                &lt;h1&gt;Apples&lt;/h1&gt;<br />
                &nbsp;&nbsp;    &lt;p&gt;Apples are fruit.&lt;/p&gt;<br />&nbsp;&nbsp;
                    <br />
                &nbsp;&nbsp;    &lt;<mark>section</mark>&gt;<br />
                &nbsp;&nbsp;    &lt;h1&gt;Taste&lt;/h1&gt;<br />
                &nbsp;&nbsp;&nbsp;&nbsp;        &lt;p&gt;They taste lovely.&lt;/p&gt;<br />
                        <br />
                &nbsp;&nbsp;&nbsp;&nbsp;        &lt;<mark>section</mark>&gt;<br />
                &nbsp;&nbsp;&nbsp;&nbsp;        &lt;h1&gt;Sweet&lt;/h1&gt;<br />
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;            &lt;p&gt;Red apples are sweeter than green ones.&lt;/p&gt;<br />
                &nbsp;&nbsp;&nbsp;&nbsp;        &lt;<mark>/section</mark>&gt;<br />
                <br />
                &nbsp;&nbsp;    &lt;<mark>/section</mark>&gt;<br />
                 <br />
                &nbsp;&nbsp;    &lt;<mark>section</mark>&gt;<br />
                &nbsp;&nbsp;    &lt;h1&gt;Color&lt;/h1&gt;<br />
                &nbsp;&nbsp;&nbsp;&nbsp;        &lt;p&gt;Apples come in various colors.&lt;/p&gt;<br />
                &nbsp;&nbsp;    &lt;<mark>/section</mark>&gt;<br />
                &lt;/body&gt;<br />            
            </code>
            
            <p>
                If you are designing a HTML5 document, 
                I would recommend you to check out <a href="http://gsnedders.html5.org/outliner/">HTML5 Outliner</a>.
                It will tell if your document is properly outlined.    
            </p>
        </article>
 <?php include("page_footer.php"); ?>   