<?php $lesson=110; ?>
<?php include("page_header.php"); ?> 
    <article>
        <header>
            <h1><?php echo $lesson_arr[$current_id][1]?></h1>
        </header>
<script type="text/javascript"><!--
google_ad_client = "ca-pub-3162865608794255";
/* Html5 below title */
google_ad_slot = "6072872264";
google_ad_width = 728;
google_ad_height = 15;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script> 


            <h2>What do we know about HTML5?</h2>
            <p>
                You probably have heard of <abbr>HTML5</abbr> by now, I guess the most popular new stuff would be
                playing video without Adobe Flash, <abbr>HTML5</abbr> evangelist should thank Mr.Steve Jobs for turning 
                it into a technology celebrity.
            </p>
            
            <p>    
                Besides, you might have heard of creating animation and playing music without plug-in, rich input control such as date picker, 
                color picker, slider without javascript, and lastly offline data storage.
            </p>
           
            <p>
                Anyway, this is just part of <abbr>HTML5</abbr>. You may want to check it out yourself from time to time -
                <a href="http://www.whatwg.org/specs/web-apps/current-work/multipage/video.html#audio"><abbr>HTML5</abbr> (including next generation additions still in development)</a>.            
            </p>
            <h2>When?</h2>
            <p>
                W3C has announced in February 2011 that July 2014 will be date that HTML5 become the official recommendation of web standard! 
            </p>
            <h2>Forget about When</h2>
            <p>
                As the matter of fact, the proposed timeline of <abbr>WHATWG</abbr> doesn't really matter. 
                The development of the web rest in the hands of two groups of people.
            </p>
            <p>    
                First group of people are those dominant web browser companies like Mozilla, Microsoft,
                 Apple, Google and Opera. The second group of people are the
                web designers or web developers in this planet. The failure of XHTML 1.1 and XHTML 2 
                is simply, web browser companies thought that was a stupid idea and they just shut their door.
            </p>
            
            <p>
                Today, most of the major web browsers are aggressively wanting to support <abbr>HTML5</abbr>. Even 
                Microsoft Internet Explorer, which has the reputation of slow in adopting new standard is supporting <abbr>HTML5</abbr> in its IE9. 
                That means, <abbr>HTML5</abbr> is going to be real and big.
            </p>
            <img class="clipart" src="images/browsers_say.png" alt="browsers want html5" title="browsers want html5"/>
            
            <aside>
                As of writing, Microsoft has announced that they have no plan to release IE9 for Windows XP users.
                If you would like to give IE9 a try, you will have to use Windows Vista SP2 or Windows 7.
            </aside>
            
            <p>
                There are no reason for web designers not to embrace <abbr>HTML5</abbr> since it makes our life easier than ever.
            </p>
            

            
        </article>
 


<?php include("page_footer.php"); ?>  