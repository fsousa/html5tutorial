<?php $lesson=400; ?>
<?php include("page_header.php"); ?> 
    <article>
        <header>
            <h1><?php echo $lesson_arr[$current_id][1]?></h1>
        </header>
<script type="text/javascript"><!--
google_ad_client = "ca-pub-3162865608794255";
/* Html5 below title */
google_ad_slot = "6072872264";
google_ad_width = 728;
google_ad_height = 15;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>         
            <p> 
                Similarly to "header" element, "footer" element is often referred to the footer of a web page.
                Well, most of the time, footer can be used as what we thought. 
            </p>
            <p>    
                A footer typically contains information about its section such as who wrote it,
                 links to related documents, copyright data, and the like. 
            </p>
            <img class="clipart" src="images/footer.jpg" />
            <p>
                Please don't think you can only have one footer per web document, you can have a footer in every section, or every article.
            </p>
            <img class="clipart" src="images/footer2.jpg" />
            
            <p>
                Please see the example below, there two article elements are footed by footer elements and there is 
                a site wide footer element which serves a main footer of the web document.
            </p>
            <code>
                &lt;!DOCTYPE HTML&gt;<br />
                &lt;HTML&gt;&lt;HEAD&gt;<br />
                &lt;TITLE&gt;The Ramblings of a Scientist&lt;/TITLE&gt;<br />
                &lt;BODY&gt;<br />
                &lt;H1&gt;The Ramblings of a Scientist&lt;/H1&gt;<br />
                <br />
                &lt;ARTICLE&gt;<br />
                 &lt;H1&gt;Episode 15&lt;/H1&gt;<br />
                 &lt;VIDEO SRC="/fm/015.ogv" CONTROLS PRELOAD&gt;<br />
                  &lt;P&gt;&lt;A HREF="/fm/015.ogv"&gt;Download video&lt;/A&gt;.&lt;/P&gt;<br />
                 &lt;/VIDEO&gt;<br />
                 <mark>&lt;FOOTER&gt;</mark> &lt;!-- footer for article --&gt;<br />
                  &lt;P&gt;Published &lt;TIME PUBDATE DATETIME="2009-10-21T18:26-07:00"&gt;&lt;/TIME&gt;&lt;/P&gt;<br />
                 <mark>&lt;/FOOTER&gt;</mark><br />
                &lt;/ARTICLE&gt;<br />
                <br />
                &lt;ARTICLE&gt;<br />

                 &lt;H1&gt;My Favorite Trains&lt;/H1&gt;<br />
                 &lt;P&gt;I love my trains. My favorite train of all time is a Kof.&lt;/P&gt;<br />
                 &lt;P&gt;It is fun to see them pull some coal cars because they look so<br />
                 dwarfed in comparison.&lt;/P&gt;<br />
                 <mark>&lt;FOOTER&gt;</mark> &lt;!-- footer for article --&gt;<br />
                  &lt;P&gt;Published &lt;TIME PUBDATE DATETIME="2009-09-15T14:54-07:00"&gt;&lt;/TIME&gt;&lt;/P&gt;<br />
                 <mark>&lt;/FOOTER&gt;</mark><br />
                &lt;/ARTICLE&gt;<br />
                <br />
                <mark>&lt;FOOTER&gt;</mark> &lt;!-- site wide footer --&gt;<br />
                 &lt;NAV&gt;<br />
                  &lt;P&gt;&lt;A HREF="/credits.html"&gt;Credits&lt;/A&gt;- <br />
                     &lt;A HREF="/tos.html"&gt;Terms of Service&lt;/A&gt; -<br />
                     &lt;A HREF="/index.html"&gt;Blog Index&lt;/A&gt;&lt;/P&gt;<br />
                 &lt;/NAV&gt;<br />
                 &lt;P&gt;Copyright &copy; 2009 Gordon Freeman&lt;/P&gt;<br />
                <mark>&lt;/FOOTER&gt;</mark><br />
                <br />
                &lt;/BODY&gt;<br />
                &lt;/HTML&gt;            
            </code>
            
        </article>
<?php include("page_footer.php"); ?>   