<?php $lesson=310; ?>
<?php include("page_header.php"); ?> 
    <article>
        <header>
            <h1><?php echo $lesson_arr[$current_id][1]?></h1>
        </header>
<script type="text/javascript"><!--
google_ad_client = "ca-pub-3162865608794255";
/* Html5 below title */
google_ad_slot = "6072872264";
google_ad_width = 728;
google_ad_height = 15;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>             
            <p>
                A Date and time field can be easily found in many web forms. Typical applications are like
                ticket booking, appointment booking, ordering pizza and etc.
            </p>
            <p>
                The most commonly used solution for date input is to use Javascript date picker. Don't believe me?
                Just google "Javascript date picker". Most of the date picker use a calendar to let user choose a
                date and fill the date into a textbox.
            </p>
            
            <p>
                As of writing, the only web browser completely support date time input is Opera (v11) and Google Chrome (v20).
                In HTML5, it is the job of web browser to ensure user can only enter a valid date time string 
                into the input textbox.
            </p>
            
            <aside>
                Picking a date from Calendar is not the only way to input a date value even though it's the most popular implementation.
                HTML5 specifications does not mention anything about displaying a calendar for date input.
            </aside>
            <table class="browser">
                <thead>
                    <tr><th>Browsers</th><th>Date Time Support</th></tr>
                </thead>
                <tbody>
                    <tr><td>IE 9 Beta</td><td></td></tr>
                    <tr><td>Firefox 13</td><td></td></tr>
                    <tr><td>Safari 5</td><td></td></tr>
                    <tr><td>Chrome 20</td><td>&#10003;</td></tr>
                    <tr><td>Opera 11</td><td>&#10003;</td></tr>
                </tbody>
            </table>
            <p>
                If you happen to have <a href="http://www.opera.com/">Opera</a> version 11 or Google Chrome 20 (or later version of course) installed in your computer, please feel free to check it out.
            </p>
            <label for="meeting">Meeting Date : </label><input id="meeting" type="date" value="2011-01-13"/>

            <p>
                If you do not have the web browser that support it, the picture below shows you how it looks like.
            </p>
            <img class="clipart" src="images/opera-calendar.png" alt="Web browser native date picker" title="Web browser native date picker" />

            <p>
                With HTML5 support, web designer no longer needs to download fancy Javascript control for basic date input.
                Exactly what you need to do is just &lt;input type="date"/&gt;   
            </p>
            
            <code>
                &lt;label for="meeting"&gt;Meeting Date : &lt;/label&gt;&lt;input id="meeting" <mark>type="date"</mark> value="2011-01-13"/&gt;            
            </code>
            
            <p>
                Once again HTML5 has made our life easier! 
                Furthermore, you are not just getting "Date" input,
                there are half a dozen of "Date Time" related inputs that you can pick and use!
                Of course, the ball is now in the court of the web browser companies as they decide when to 
                implement this standard.
            </p>
                <section>
                    <h1>1. Date (&lt;input type="date"/&gt;)</h1>
                    <p>
                        Apparently this is browser native date picker. You can only pick a specific date from the calendar.
                    </p>
                    <img alt="Opera Date Picker" src="images/opera-calendar.png" />
                </section>

                <section>
                   <h1>2. Week (&lt;input type="week"/&gt;)</h1>
                   <p>
                       Instead of picking a date, you can pick a week too.
                       Please notice the Week number on the left of the calendar.
                   </p>
                   <img alt="Opera Week Picker" src="images/opera-week.png" />
                </section>

                <section>
                   <h1>3. Month (&lt;input type="month"/&gt;)</h1>
                   <p>You can even have a month picker, here the calendar that allows you to choose a month in a year.</p> 
                   <img alt="Opera Month Picker" src="images/opera-month.png" />
                </section>

                <section>
                   <h1>4. Time (&lt;input type="time"/&gt;)</h1>
                   <p>This is nothing special, a time picker for time input.</p>
                   <img alt="Opera Time Picker" src="images/opera-time.png" />
                </section>

                <section>
                   <h1>5. Date and Time (&lt;input type="datetime"/&gt;)</h1>
                    <p>You can choose date and time with time zone. Input value is represented in UTC time.</p>
                    <img alt="Opera Date Time Picker (UTC)" src="images/opera-datetime.png" />
                </section>

                <section>
                    <h1>6. Local Date and Time (&lt;input type="datetime-local"/&gt;)</h1>
                    <p>In compared to UTC time,  you can have input date time value represents local time too. </p>
                    <img alt="Opera Local Date Time Picker" src="images/opera-datetime-local.png" /> 
                </section>
            <p>
                Let's look into the related attributes that we possibly use.    
            </p>
<script type="text/javascript"><!--
google_ad_client = "ca-pub-3162865608794255";
/* Html5 below title */
google_ad_slot = "6072872264";
google_ad_width = 728;
google_ad_height = 15;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>            
            <table class="attribute">
                <thead>
                    <tr>
                        <th>Attributes</th>
                        <th>Descriptions</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>value</td>
                        <td>Value is the default value of the input box when a page is first loaded. 
                        This is a common attribute for &lt;input&gt; element regardless which type you are
                        using.
                        </td>
                    </tr>                  
                    <tr>
                        <td>min</td>
                        <td>Minimum Date or time</td>
                    </tr>
                    <tr>
                        <td>max</td>
                        <td>Maximum Date or time</td>
                    </tr>
                    <tr>
                        <td>step</td>
                        <td>Step scale factor.
                        Different type of input has its own default "step" value.
                        <ul>
                            <li>Date - default is 1 day</li>
                            <li>Week - default is 1 week</li>
                            <li>Month - default is 1 month</li>
                            <li>Time - default is 1 minute</li>
                            <li>DateTime - default is 1 minute</li>
                            <li>Local DateTime - default is also 1 minute</li>
                        </ul>
                        
                        </td>
                    </tr>
                </tbody>
            </table>
        </article>
<?php include("page_footer.php"); ?>   