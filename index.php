<?php $lesson=100; ?>
<?php include("page_header.php"); ?> 
    <article>
        <header>
            <h1><?php echo $lesson_arr[$current_id][1]?></h1>
        </header>

            <img class="clipart" src="images/HTML5_Logo_128.png" style="float:left" alt="HTML5 Official logo" title="HTML5 Official Logo"/>
            <p>
                I believe <abbr>HTML5</abbr> is will be something big (many people believe so too). It might not change 
                the way we eat or dress, but it certainly will change the way people develop web application and
                designing web pages
                for the next 10 years.
            </p>
            <p>    
                However, this is a not a HTML 101 tutorial. HTML5 is not something completely new. Most of 
                HTML5 specifications came from HTML 4 or XHTML 1.0. With my limited time and energy, I will only 
                cover what is new in HTML5 and old practice that we shall not be using any longer.
            </p>            
            <p>
                This is a <abbr>HTML5</abbr> tutorial site. While I am exploring and learning HTML5, I hope I am 
                able to share what I've learned freely.
            </p>

            <blockquote>
            <p>
            There are two kinds of impulses, corresponding to the two kinds of goods. 
            <img src="images/bertrand_russell.jpg" class="clipart" style="float:right" alt="Bertrand Russell" title="Bertrand Russell" />
            
            There are possessive impulses, which aim at acquiring or retaining private goods that 
            cannot be shared; these center in the impulse of property. 
            And there are creative or constructive impulses, which aim at bringing into the world or 
            making available for use the kind of goods in which there is no privacy and no possession.
            </p>
            <p>
            The best life is the one in which the creative impulses play the largest part and the 
            possessive impulses the smallest. ~ <cite>Bertrand Russell</cite> (1872 - 1970)
            </p>
            
            </blockquote>
            <p>
                As of this writing, <abbr>HTML5</abbr> is still a working draft. In another word, <abbr>HTML5</abbr> is still a work in progress thing.
                The specifications that published in <a href="http://www.whatwg.org/specs/web-apps/current-work/multipage/">W3C</a> 
                grows and change from day to day.
            </p>
            
            <p>
                Very much the same for me in constructing this tutorial, I decided not to complete the entire tutorial before I publish it,
                instead, I would upload bit by bit every other day like blogs entry.
            </p>


    </article>
<?php include("page_footer.php"); ?>   
