<?php $lesson=350; ?>
<?php include("page_header.php"); ?> 
    <article>
        <header>
            <h1><?php echo $lesson_arr[$current_id][1]?></h1>
        </header>
<script type="text/javascript"><!--
google_ad_client = "ca-pub-3162865608794255";
/* Html5 below title */
google_ad_slot = "6072872264";
google_ad_width = 728;
google_ad_height = 15;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script> 
            <p> 
                The time element represents either a time on a 24 hour clock, 
                or a precise date in the proleptic <a href="http://en.wikipedia.org/wiki/Gregorian_calendar" rel="archives" >Gregorian calendar</a>, 
                optionally with a time and a time-zone offset. 
            </p>
            <p>
                For instance, you can do this to indicate a specific date.
            </p>
            <code>
                We were celebrating our daugther's &lt;time datetime="2004-07-13"&gt;birthday&lt;/time&gt; 
                in that seafood restaurant.
            </code>
            
            <p>
                In the example below, since the time value is exposed to user in 24 hour format, 
                we do not need "datetime" attribute to further specify the time value.
            </p>
            
            <code>
                I usually go swimming at &lt;time&gt;20:00&lt;/time&gt; every Saturday.
            </code>
            
            <p>
                Using a styling technology that supports restyling times, 
                the sentence above can be
            </p>
            <p>
                "I usually go swimming at 8pm every Saturday."
            </p>
            <p> Or </p>
            <p>
                "I usually go swimming at 20h00 every Saturday."
            </p>
            
            <p>
                On the hand, if the time value is not exposed in 24 hour format, you will have to specifically 
                encode it in "datetime" attribute in 24 hour format.
            </p>
            
            <code>
                We are open Monday to Friday from &lt;time datetime="08:00"&gt;8am&lt;/time&gt; through &lt;time datetime="18:00"&gt;6pm&lt;/time&gt;.
            </code>
            
            <p>
                For a specific date and time (local time), here is how we do
            </p>
            
            <code>
                You may collect your book at &lt;time datetime="2011-01-16T06:00Z"&gt;January 16, 2011 6pm&lt;/time&gt;.
            </code>
            
            <p>
                Alternatively, you may need to specify an accurate date and time with time zone too.
            </p>
            
            <code>
                The webinar will start at &lt;time datetime="2011-02-22T09:00-4:00"&gt;February 22, 2011 9am EDT&lt;/time&gt;.
            </code>
            
            <p>
                The HTML5 specification tells us not to use "time" element if precise date or time is not known. For instance,
            </p>
            
            <p>
                "I know what you did last Summer" or "I knew him sometime around 1980"
            </p>
            
            <p>
                You may ask what do we get at the end of the day for using the "time" element. Alright, 
                with all the effort, we are making our content machine readable, 
                imagine we can one day save the "February 22, 2011 9am EDT" into our Calendar with just one click.
            </p>
            
            <p>
                Besides the "datetime" attribute, the "time" element has two more attributes "pubdate" and "valueAsDate".
            </p>
            <table class="attribute">
                <thead>
                    <tr>
                        <th>Attributes</th>
                        <th>Descriptions</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>dateTime</td>
                        <td>Date or Time or Date Time value. 
                            If present, gives the date or time being specified. 
                             Otherwise, the date or time is given by the element's contents.
                        </td>
                    </tr>                  
                    <tr>
                        <td>pubDate</td>
                        <td>Publication Date. This is boolean attribute, which means you don't need put a value to it.
                            If specified, it means the publication date of an article (&lt;article&gt;),
                            if "time" element is not within an article block, it means it is the publication date of 
                            the entire document.<br />
                            
                            Note: You should only have one "pubdate" in one article or one document.
                        </td>
                    </tr>
                    <tr>
                        <td>valueAsDate</td>
                        <td>This is a readOnly attribute, it returns a Date object representing the specified date and time</td>
                    </tr>
                </tbody>
            </table>            
            
            <p>
                "pubDate" can be typically applied in blog entry or news article.
                For article that published in January 21 2011, simply encode as : 
            </p>
            
            <code>
                &lt;time datetime="2011-01-21" pubdate&gt;January 21 2011&lt;/time&gt;
            </code>

        </article>
<?php include("page_footer.php"); ?>   