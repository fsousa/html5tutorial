<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="keywords" content="HTML, HTML5, tutorial, HTML5 Tutorial, CSS, CSS3, Webform 2.0, Semantic, Canvas, Audio, Video, DataList, Required field, Autofocus" />
<meta name="author" content="Teng-Yong Ng" />
<link href="images/favicon.ico" rel="icon" type="image/x-icon"/>
<link rel="stylesheet" href="tutorial.css" />

<!--[if IE]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

<?php
    //config start
    $site_title="HTML5 Tutorial";
    $site_subtitle ="Learn HTML5 in plain english";
    //config end

    include ('tutorial_link.php'); //lesson page
    
    $current_id = array_search($lesson,$tutarray);
    $prev=$current_id-1;
    $next=$current_id+1; 
?>
<meta name="Description" content="<?php echo 'HTML5 Tutorial - '.$lesson_arr[$current_id][3]?>"/>
<title><?php 
if ($non_lesson)
	echo $page_title;
else
	echo $site_title." - ".$lesson_arr[$current_id][1];

?></title>

<script type="text/javascript">
    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-292947-15']);
    _gaq.push(['_trackPageview']);

    (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();
</script>
<script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#pubid=ra-4e489b5533393067"></script>
</head>
<body>
  
    <div id="main">
        <!-- Branding -->
        <header>
					<a href="/">
                    <div id="logo">
                        <h1><?php echo $site_title;?></h1>
				    </div>
                    </a>
                


				<script type="text/javascript"><!--
				google_ad_client = "ca-pub-3162865608794255";
				/* HTML5 banner */
				google_ad_slot = "7501318944";
				google_ad_width = 728;
				google_ad_height = 90;
				//-->
				</script>
				<script type="text/javascript"
				src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
				</script>

        </header>
        <!-- Branding -->
        
        <!-- Side Navigator -->
        <?php
            $nav_class="navi_left";
            if ($prev<0)
                $nav_class = "navi_left top";
            
            if ($next>=count($lesson_arr))
                $nav_class = "navi_left bottom";
        ?>

        <nav class ="<?php echo $nav_class ?>">
            <ol>
                <?php
                    foreach ($lesson_arr as $key => $value)
                    {
                        $text = $lesson_arr[$key][1];
                        $url = $lesson_arr[$key][2];
                        $level = $lesson_arr[$key][4];
                        $output='';
                        if ($non_lesson!=true && $current_id == $key)
                            $output='<li class="selected '.$level.'">'.$text.'</li>';
                        else
                            $output='<li class="'.$level.'"><a href="'.$url.'">'.$text.'</a></li>';
                        echo $output;      
                    }
                 ?>         
            </ol>
        </nav>
        <!-- Side Navigator -->
        



 