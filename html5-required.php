<?php $lesson=250; ?>
<?php include("page_header.php"); ?> 
    <article>
        <header>
            <h1><?php echo $lesson_arr[$current_id][1]?></h1>
        </header>
<script type="text/javascript"><!--
google_ad_client = "ca-pub-3162865608794255";
/* Html5 below title */
google_ad_slot = "6072872264";
google_ad_width = 728;
google_ad_height = 15;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>             
            <p>
                A "Required Field" is a field that must be filled in with value before submission of a form.
                Required field sometimes known as Mandatory field or Compulsory field.
            </p>
            
            <p>
                As of writing, only Opera and Firefox support "Required" attribute of input textbox.
                Please see table below.
            </p>    
 

            <table class="browser">
                <thead>
                    <tr><th>Browsers</th><th>"Required" Support</th></tr>
                </thead>
                <tbody>
                    <tr><td>IE 9 Beta</td><td></td></tr>
                    <tr><td>Firefox 4</td><td>&#10003;</td></tr>
                    <tr><td>Safari 5</td><td></td></tr>
                    <tr><td>Chrome 20</td><td>&#10003;</td></tr>
                    <tr><td>Opera 9</td><td>&#10003;</td></tr>
                </tbody>
            </table>
            
            <p>
                To make a field as required field is fairly simple, you are only required to add a "required" boolean attribute into your input element.
            </p>
            
            <code>
           &lt;form&gt;<br />
                &lt;label for="movie"&gt;What is your favorite movie : &lt;/label&gt;<br />
                &lt;input name="movie" type="text" <mark>required</mark>  /&gt;<br />
                &lt;input type="submit" value="Submit"/&gt;            <br />
            &lt;/form&gt;            
            
            </code>
            
            <p>
                The picture below shows us how Firefox and Opera prompt user to fill in value if a "Required field" 
                is left blank upon submission.
            </p>
            
            <img class="clipart" src="images/required_gallery.png" alt ="How browser prompt input for required field" title = "How browser prompt input for required field" />
            
            <p>Below is the demo of "Required field", feel free to test it with your browser.</p>
            <form>
                <label for="movie">What is your favorite movie : </label>
                <input name="movie" required type="text" />
                <input type="submit" value="Submit"/>            
            </form>
        </article>
 <?php include("page_footer.php"); ?>   