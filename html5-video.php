﻿<?php $lesson=150; ?>
<?php include("page_header.php"); ?> 
    <article>
        <header>
            <h1><?php echo $lesson_arr[$current_id][1]?></h1>
        </header>
<script type="text/javascript"><!--
google_ad_client = "ca-pub-3162865608794255";
/* Html5 below title */
google_ad_slot = "6072872264";
google_ad_width = 728;
google_ad_height = 15;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>             
            <p>
                Video streaming over the internet is nothing new, the attempt in doing so can be dated back to the mid of 20th century. 
                Due to the limited network bandwidth and lower CPU power, nothing was quite successful until Youtube made its debut on 2005.
            </p>
            <p>
                With <abbr>HTML5</abbr>, we can now "embed" video as a native element as &lt;img&gt;. 
                It works almost the same as &lt;audio&gt; besides it has three attributes that only applicable to &lt;video&gt;. 
                There are "width", "height" and "poster"."width" and "height" determine the screen size and "poster" is just
                a still image file projected on screen before the movie gets played.
            </p>
            
            <h2>How to?</h2>
            <p>The code to stream video from your web page can be as simple as the line below.</p>
            <p>
            <code>
                &lt;video src="rain.mp4" controls width="480" height="360"&gt;&lt;/video&gt;            
            </code>
            </p>
           
            <p>
                In reality, life isn't as how we thought it should be. The simple line of code above works only in Google Chrome,
                Apple Safari, and yet for Apple Safari to play the video, you have got to make sure QuickTime is installed in your computer.
            </p>
            
            <p>
                Similarly to the plight of audio, we don't have an universal video format that all web browsers are supporting. 
                At least at the time of writing (13 December 2010).
            </p>
            <p>
                Again, it is money games at the end of the day. H.264 is patent-encumbered, 
                Firefox and Opera chose to embrace a royalty free codec which is Theora and WebM (VP8 codec)
            </p>
            <table class="browser">
                <thead>
                    <tr>
                        <th>Browser</th>
                        <th>H.264+<br />AAC+<br />MP4</th>
                        <th>WebM</th>
                        <th>Theora<br />+Vorbis<br />+Ogg</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Mozzila Firefox 3.6</td>
                        <td></td>
                        <td>&#10003;</td>
                        <td>&#10003;</td>
                    </tr>
                     <tr>
                        <td>Opera 10.63</td>
                        <td></td>
                        <td>&#10003;</td>
                        <td>&#10003;</td>
                    </tr>                   
                    <tr>
                        <td>Google Chrome 8.0</td>
                        <td>&#10003;</td>
                        <td>&#10003;</td>
                        <td>&#10003;</td>
                    </tr>
                    <tr>
                        <td>Apple Safari 5.0.3 (with QuickTime)</td>
                        <td>&#10003;</td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Microsoft IE 9 Beta</td>
                        <td>&#10003;</td>
                        <td>&#10003;</td>
                        <td></td>
                    </tr>                                                                
                </tbody>
            </table>
            <aside>
                Google has anounced in Jan 2011 to withdraw the support of H.264 in future version of Google Chrome.
            </aside>
            <p>
                In short, the simple single line code as I demonstrated above isn't practical. You've got to provide at least two sources (recommended is three) for one video. 
            </p>
            <p>
            <code>
                &lt;video controls width="480" height="288" &gt;<br />
                    &nbsp;&nbsp;&lt;source src="media/html5iscool.mp4" type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"' /&gt;<br />
                    &nbsp;&nbsp;&lt;source src="media/html5iscool.ogv" type='video/ogg; codecs="theora, vorbis"' /&gt;<br />
                    &nbsp;&nbsp;&lt;source src="media/html5iscool.webm" type='video/webm; codecs="vp8, vorbis"'/&gt;<br />
                &lt;/video&gt;            
            </code>
            </p>
            
            
           <aside>
                Tips: 
                <p>
                    To encode you video file into H.264 video, you may want to try out <a href="http://handbrake.fr/">HandBrake</a>, a decent GPL-licensed and open source tool.
                    If you wanna encode for only one video and reluctant to install another application into your already clogging OS, you can upload your video to Youtube, Youtube will then 
                    encode your video into H.264 format and you can then download from there.
                </p>
                <p>
                    As for WebM (.webm) and Theora (.ogv) format, you may try <a href="http://www.firefogg.org/">Firefogg</a>, a firefox plug-in that require minimal of download.
                </p>

           
           </aside> 
            
            
            <p><h2>Attributes of &lt;video&gt;</h2></p>
            
            <table class="attribute">
                <thead>
                    <tr>
                        <th id="att_name">Attribute</th>
                        <th id="att_value">Value</th>
                        <th id="att_desc">Description</th>
                    </tr>
                 </thead>
                <tbody>
                    <tr>
                        <td>controls</td>
                        <td>*Boolean attribute</td>
                        <td>If you want the browser native player control to be around, specifiy "controls" </td>
                    </tr>
                    <tr>
                        <td>autoplay</td>
                        <td>*Boolean attribute</td>
                        <td>If this guy exists, the browser will just play your video without asking permission from visitor</td>
                    </tr>
                    <tr>
                        <td>loop</td>
                        <td>*Boolean attribute</td>
                        <td>Keep repeating your video</td>
                    </tr> 
                    <tr>
                        <td>src</td>
                        <td>url</td>
                        <td>The URL of your video file</td>
                    </tr> 
                    <tr>
                        <td>preload</td>
                        <td>none | metadata | auto</td>
                        <td>
                            This attribute was formerly known as "autobuffer" and it was an boolean attribute as "controls".<br /><br />
                            none - do not buffer video file automatically.<br />
                            metadata - only buffer the metadata of video<br />
                            auto - buffer video file before it gets played.
                        </td>
                    </tr>
                    <tr>
                        <td>width</td>
                        <td>width in pixels</td>
                        <td>Width of video player</td>
                    </tr>
                    <tr>
                        <td>height</td>
                        <td>height in pixels</td>
                        <td>Height of video player</td>
                    </tr>
                    <tr>
                        <td>poster</td>
                        <td>url of a image file</td>
                        <td>If present, shows the poster image until the first frame of video has downloaded.</td>
                    </tr>                                                                                                                                                
                </tbody>
            </table>            
            
            <h2>What about browsers that are <abbr>HTML5</abbr> illiterate?</h2>
            <p>
                If you think encoding one video for three times is painful, hold your breath for a little longer, you are
                going to take more pain in order to accomodate more visitors who are using an less modern web browser. i.e. Internet Explorer 8 and below.
            </p>
            <p>
                Microsoft has released its Internet Explorer 9 Beta on Nov 2010 which supporting <abbr>HTML5</abbr>, but &lt;video&gt; tag is yet supported as of writing.
                Hence, you will have to take step further to embed a flash player in &lt;object&gt; since most user has flash plugin installed. 
                I believe Microsoft will eventually support &lt;video&gt;, even so, its always taking time for majority of the user to migrate to the latest version of web browser.
            </p>    
            <p>    
                <code>
                    &lt;video controls width="320" height="240"&gt;<br />
                        &lt;source src="media/html5iscool.mp4" type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"' /&gt;<br />
                        &lt;source src="media/html5iscool.ogv" type='video/ogg; codecs="theora, vorbis"' /&gt;<br />
                        &lt;source src="media/html5iscool.webm" type='video/webm; codecs="vp8, vorbis"'/&gt;<br />
                        &lt;object width="320" height="240" type="application/x-shockwave-flash" data="media/flowplayer-3.2.5.swf"&gt;  <br />
                        &nbsp;&nbsp    &lt;param name="movie" value="media/flowplayer-3.2.5.swf" /&gt;  <br />
                        &nbsp;&nbsp    &lt;param name="allowfullscreen" value="false" /&gt; <br /> 
                        &nbsp;&nbsp    &lt;param name="flashvars" <br />
                            value='config={"clip": {"url": "html5iscool.mp4", "autoPlay":false, "autoBuffering":true}}' /&gt;<br />  
                        &lt;/object&gt; <br />                   
                    &lt;/video&gt;                
                </code>
            </p>
            
            <p></p>
            
            
                <video controls width="320" height="240">
                    <source src="media/html5iscool.mp4" type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"' /> 
                    <source src="media/html5iscool.ogv" type='video/ogg; codecs="theora, vorbis"' /> 
                    <source src="media/html5iscool.webm" type='video/webm; codecs="vp8, vorbis"'/> 

                    <object width="320" height="240" type="application/x-shockwave-flash" data="media/flowplayer-3.2.5.swf">  
                        <param name="movie" value="media/flowplayer-3.2.5.swf" />  
                        <param name="allowfullscreen" value="false" />  
                        <param name="flashvars" 
                        value='config={"clip": {"url": "html5iscool.mp4", "autoPlay":false, "autoBuffering":true}}' />  
                    </object>                    
                </video>
                
            <aside>
                Tips: The flash player in above sample code can be downloaded freely at <a href="http://flowplayer.org/download/index.html">Flowplayer</a>
            </aside>                
        </article>
<?php include("page_footer.php"); ?>   