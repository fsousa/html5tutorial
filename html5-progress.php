<?php $lesson=370; ?>
<?php include("page_header.php"); ?> 
    <article>
        <header>
            <h1><?php echo $lesson_arr[$current_id][1]?></h1>
        </header>
        <script type="text/javascript"><!--
google_ad_client = "ca-pub-3162865608794255";
/* Html5 below title */
google_ad_slot = "6072872264";
google_ad_width = 728;
google_ad_height = 15;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script> 
            <p>
                The new "progress" element appears to be very similar to the "<a href="html5-meter.php">meter</a>" element.
                It is created to indicate progress of a specific task.
            </p>
            <p>
                The progress can be either determinate OR interderminate. Which means, you can use "progress" element
                to indicate a progress that you do not even know how much more work is to be done yet.
            </p>
            <p>
                Sounds confusing? How can you tell a progress without even know where the end is? 
            </p>
            <p>
                The first possibility, the work hasn't even started. 
                So, it is 0 percent or 0.0001 percent which is not relevant at all.
            </p>
            <p>
                The second possibility is, the actual value of the 
                progress is in waiting from some servers or processes to update it
                on the fly.
            </p>
            <p>
                One of the typical usage of "progress" element is progress of file download.
            </p>
            <code>
                &lt;section&gt;<br />
                &lt;p&gt;Progress: &lt;progress id="p" max=100&gt;&lt;span&gt;0&lt;/span&gt;%&lt;/progress&gt;&lt;/p&gt;<br />
                <br />
                &lt;script&gt;<br />
                  var progressBar = document.getElementById('p');<br /><br />
                  function updateProgress(newValue) {<br />
                    progressBar.value = newValue;<br />
                    progressBar.getElementsByTagName('span')[0].textContent = newValue;<br />
                  }
                 &lt;/script&gt;<br />
                &lt;/section&gt; <br />         
            </code>
            
            <p>
                For determinate progress, you can always set the value of progress in "value" attribute. There is 
                "max" attribute for you to indicate the maximum value of the progress. In compared to "meter" element,
                there isn't a "min" attribute as the minimum of progress is always 0 (zero).    
            </p>
            <code>
                Progress of Task A : &lt;progress value="60" max="100"&gt;60%&lt;/progress&gt;
            </code>
            
            <p> 
                Progress of Task A : <progress max="100" value="60">60%</progress>
            </p>
            
            <p>
                As of writing, only Google Chrome 8 and Opera 11 render "progress" element as progress bar.
            </p>

            <table class="browser">
                <thead>
                    <tr><th>Browsers</th><th>Progress element support</th></tr>
                </thead>
                <tbody>
                    <tr><td>IE 9 Beta</td><td></td></tr>
                    <tr><td>Firefox 13</td><td>&#10003;</td></tr>
                    <tr><td>Safari 5</td><td></td></tr>
                    <tr><td>Chrome 8</td><td>&#10003;</td></tr>
                    <tr><td>Opera 11</td><td>&#10003;</td></tr>
                </tbody>
            </table>
            
            <p>
                If you are not using any of web browser that supports "progress" element, below picture shows you 
                how it looks like in Google Chrome 8.
            </p>            
            
            <img class="clipart" src="images/progress.png" alt="progress element in Google Chrome"/>
        </article>
<?php include("page_footer.php"); ?>   
