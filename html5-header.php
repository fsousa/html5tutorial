<?php $lesson=390; ?>
<?php include("page_header.php"); ?> 
    <article>
        <header>
            <h1><?php echo $lesson_arr[$current_id][1]?></h1>
        </header>
<script type="text/javascript"><!--
google_ad_client = "ca-pub-3162865608794255";
/* Html5 below title */
google_ad_slot = "6072872264";
google_ad_width = 728;
google_ad_height = 15;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>         
            <p> 
                Almost every modern website has a header section which shows up on the top in almost every page within the website.
            </p>
            
            <p>
                The new "header" element is meant for that.
            </p>
            <img class="clipart" src="images/header.jpg" alt="masthead of website"/>
            
            <p>
                Actually, I was just joking. "header" element is meant more than just that.
                We are encouraged to use use "header" element for so-called 
                masthead (master head) of a website.
            </p>
            
            <p>
                By the way, I didn't mean "New York Times" actually uses "header" element. I mean "New York Times" can
                use "header" element for that.
            </p>
            
            <code>
                &lt;body&gt;<br />
                 <mark>&lt;header&gt;</mark><br />
                  &lt;h1&gt;Little Green Guys With Guns&lt;/h1&gt;<br />
                  &lt;nav&gt;<br />
                   &lt;ul&gt;<br />
                    &lt;li&gt;&lt;a href="/games"&gt;Games&lt;/a&gt;<br />
                    &lt;li&gt;&lt;a href="/forum"&gt;Forum&lt;/a&gt;<br />
                    &lt;li&gt;&lt;a href="/download"&gt;Download&lt;/a&gt;<br />
                   &lt;/ul&gt;<br />
                  &lt;/nav&gt;<br />
                 <mark>&lt;/header&gt;</mark><br />
                 &lt;p&gt;You have three active games:&lt;/p&gt;<br />
                 &lt;!-- this is still part of the subsection entitled "Games" --&gt;<br />
                 ...            
            </code>
            
            <p>
                However, we mustn't think that "header" is only for masthead of a website. "header"
                can be use as a heading of an blog entry or news article 
                as every article has its title and published date and time.
            </p>
            
            <code>
            &lt;article&gt;<br />
                <mark>&lt;header&gt;</mark><br />
                    &lt;h1&gt;Military Offers Assurances to Egypt and Neighbors&lt;/h1&gt;<br />
                    Published : &lt;time datetime="2011-02-13" pubdate&gt;February 13 2011&lt;/time&gt;)<br />
                <mark>&lt;/header&gt;</mark><br />
                &lt;p&gt;<br />
                    CAIRO - As a new era dawned in Egypt on Saturday, <br />
                    the army leadership sought to reassure Egyptians and <br />
                    the world that it would shepherd a transition to civilian <br />
                    rule and honor international commitments like the peace treaty with Israel.<br />
                &lt;/p&gt;<br />
            &lt;/article&gt;            
            </code>
            
            <img class="clipart" src="images/header2.jpg" />
            <p>
                Not only that, we mentioned in previous "<a href="html5-section.php">section</a>",
                each section should have probably have its heading.
            </p>
            
            <code>
              &lt;section&gt;<br />
              <mark>&lt;header&gt;</mark><br />
                    &lt;h1&gt;Application&lt;/h1&gt;<br />
                    &lt;h2&gt;Paid iphone apps&lt;/h2&gt;<br />
                &lt;/hgroup&gt;<br />
              <mark>&lt;/header&gt;</mark><br />
              <br />
              &lt;ul&gt;<br />
               &lt;li&gt;Education&lt;/li&gt;<br />
               &lt;li&gt;Entertainment&lt;/li&gt;<br />
               &lt;li&gt;Family&lt;/li&gt;<br />
               &lt;li&gt;Games&lt;/li&gt;<br />
               &lt;li&gt;News and Weather&lt;/li&gt;<br />
              &lt;/ul&gt;<br />
            &lt;/section&gt;            
            </code>
            
            <p>
                The "header" element usually contain header elements (h1 to h2 or hgroup), however
                this is not a requirement, even it is commonly practised. 
            </p>
            
            <p>
                Similarly to what we have learnt in "<a href="html5-section.php">section</a>", "header" element
                has its own outline, you can happily define your own h1,h2..h6 without worried what has been defined
                in its ancestor.
            </p>
            
        </article>
<?php include("page_footer.php"); ?>   